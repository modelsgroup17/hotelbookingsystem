package se.chalmers.cse.mdsd1617.yakindu;

class Reservation {
	
	private final long id;
	private ReservationStatus status = ReservationStatus.CREATED;
	private Room room = null;
	
	public Reservation(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setStatus(ReservationStatus status) {
		this.status = status;
		if (room != null) {
			room.setStatus(status.getRoomStatus());
		}
	}
	
	public ReservationStatus getStatus() {
		return status;
	}
	
	public void setRoom(Room room) {
		this.room = room;
	}
	
	public Room getRoom() {
		return this.room;
	}
}
