package se.chalmers.cse.mdsd1617.yakindu;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

// @todo Javadoc everything
// @todo Room-class, add all helpers in that class. Makes more sense.
public class Assignment3Complete {
	
	private long currentBookingId = 0;
	private long currentReservationNumber = 0;
	
	private final int MAX_ROOMS = 2;

	/**
	 * Maps bookings to reservations, one-to-many.
	 */
	private Map<Long, List<Reservation>> bookingToReservations = new HashMap<>();
	
	/**
	 * A list of available rooms and their ids. As of right now,
	 * we only model two rooms, hence two at most is available.
	 */
	private Set<Room> availableRooms = new LinkedHashSet<>(Arrays.asList(new Room(0L),  new Room(1L)));
	
	public long initiateBooking() {
		return ++currentBookingId;
	}
	
	public int reservationsForBooking(long bookingId) {
		List<Reservation> reservations = bookingToReservations.get(bookingId);
		return reservations == null ? 0 : reservations.size();
	}
	
	private int numberOfReservations() {
		int n = 0;
		for (List<Reservation> reservations : bookingToReservations.values()) {
			n += reservations.size();
		}
		return n;
	}

	public boolean addReservationToBooking(long bookingId) {	
		if (bookingId < 0 || bookingId > currentBookingId || numberOfReservations() >= MAX_ROOMS) {
			return false;
		} else {

			List<Reservation> current = bookingToReservations.get(bookingId);
			if (current == null) {
				current = new ArrayList<Reservation>();
			}
			
			current.add(new Reservation(currentReservationNumber));
			bookingToReservations.put(bookingId, current);
			assignRoomToReservation(bookingId, currentReservationNumber);

			currentReservationNumber = (currentReservationNumber + 1) % 2;
			return true;
		}
	}
	
	private Reservation getReservationWithId(List<Reservation> list, long id) {
		for (Reservation r : list) {
			if (r.getId() == id) {
				return r;
			}
		}
		return null;
	}
	
	/**
	 * Resets the internal state of bookings/reservations/rooms.
	 * @param bookingId
	 */
	public boolean checkoutReservation(Long bookingId, Long reservationId) {
		List<Reservation> reservations = bookingToReservations.get(bookingId);
		Iterator<Reservation> iter = reservations.iterator();
		while (iter.hasNext()) {
			Reservation reservation = iter.next();
			if (reservation.getId() == reservationId && reservation.getStatus() == ReservationStatus.CREATED) {
				// Mark room as free again internally
				if (reservation.getRoom() != null) {
					availableRooms.add(reservation.getRoom());
				}
				
				// Remove reservation
				iter.remove();
				bookingToReservations.put(bookingId, reservations);	
			}
		}
		
		return true;
	}
	
	/**
	 * Assigns a specified reservation to an available room.
	 * 
	 * @param reservationId
	 */
	public void assignRoomToReservation(Long bookingId, long reservationId) {
		// @todo Used in code as raise, which doesn't work. Can we do it directly?
		// @todo If we do; we can pass reservation directly as an instance.
		Room room = availableRooms.iterator().next();
		availableRooms.remove(room);
		
		Reservation reservation = getReservationWithId(bookingToReservations.get(bookingId), reservationId);
		reservation.setRoom(room);
	}
	
	/**
	 * @param bookingId
	 * @param reservationId
	 * @return Whether the specified reservation belongs to the specified booking
	 */
	public boolean reservationBelongsToBooking(long bookingId, long reservationId) {
		List<Reservation> reservations = bookingToReservations.get(bookingId);
		if (reservations == null) {
			return false;
		}
		for (Reservation r : reservations) {
			if (r.getId() == reservationId) {
				return true;
			}
		}
		return false;
	}

	public boolean roomBelongsToBooking(long bookingId, long roomId) {
		// @todo Stupid?
		return reservationBelongsToBooking(bookingId, roomId);
	}
	
	// @todo free/assign/checkIn... nextRoom are all similar and can be extracted to a helper method.
	// !!!!
	
	public boolean confirmAllReservations(Long bookingId) {
		for (Reservation r : bookingToReservations.get(bookingId)) {
			r.setStatus(ReservationStatus.CONFIRMED);
		}
		return true;
	}
	
	public boolean checkInAllReservations(Long bookingId) {
		for (Reservation r : bookingToReservations.get(bookingId)) {
			if (r.getStatus() == ReservationStatus.CONFIRMED) {
				r.setStatus(ReservationStatus.CHECKED_IN);
			}
		}
		return true;
	}
	
	public boolean checkOutAllReservations(Long bookingId) {
		for (Reservation r : bookingToReservations.get(bookingId)) {
			if (r.getStatus() == ReservationStatus.CHECKED_IN) {
				r.setStatus(ReservationStatus.CHECKED_OUT);
			}
		}
		return true;
	}
	
	/**
	 * Checks in the next rooms for the booking.
	 * @param bookingId
	 * @return Always true
	 */
	public boolean checkInOneReservation(long bookingId) {
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if (reservation.getStatus() == ReservationStatus.CONFIRMED) {
				reservation.setStatus(ReservationStatus.CHECKED_IN);
				return true;
			}
		}
		
		return true;
	}
	
	/**
	 * Frees the next room for the booking.
	 * @param bookingId
	 * @return Always true
	 */
	public boolean checkOutOneReservation(long bookingId) {
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if (reservation.getStatus() == ReservationStatus.CHECKED_IN) {
				reservation.setStatus(ReservationStatus.CHECKED_OUT);
				return true;
			}
		}
		
		return true;
	}
	
	/**
	 * Pays for the next room for the booking.
	 * @param bookingId
	 * @return Always true
	 * @todo Delete?
	 */
	public boolean payForOneReservation(long bookingId) {
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if (reservation.getStatus() == ReservationStatus.CHECKED_OUT) {
				reservation.setStatus(ReservationStatus.CREATED);
				return true;
			}
		}
		return true;
	}
	
	public boolean payForAllReservations(long bookingId) {
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if (reservation.getStatus() == ReservationStatus.CHECKED_OUT) {
				reservation.setStatus(ReservationStatus.CREATED);
			}
		}
		return true;
	}
	
	
	private List<Reservation> getReservationsWithStatus(ReservationStatus status, long bookingId) {
		List<Reservation> matches = new ArrayList<>();
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if(reservation.getStatus() == status) {
				matches.add(reservation);
			}
		}
		return matches;
	}
	
	/**
	 * Checks if there is a reservation that is checked in when the user wants to pay for the other reservation.
	 * @param bookingId
	 * @return Whether one of the reservations has the status CHECKED_IN.
	 */
	public boolean checkIfReservationHasCheckedInRooms(long bookingId) {
		return getReservationsWithStatus(ReservationStatus.CHECKED_IN, bookingId).size() > 0;
	}
	
	/**
	 * Checks if there is a reservation that is confirmed when the user wants to pay for the other reservation.
	 * @param bookingId
	 * @return Whether one of the reservations has the status CONFIRMED.
	 */
	public boolean checkIfReservationHasConfirmedRooms(long bookingId) {
		return getReservationsWithStatus(ReservationStatus.CONFIRMED, bookingId).size() > 0;
	}
	
	//create the last method that will pay for one room if both rooms are in the state CHECKED_OUT
	
	public boolean checkIfAllRoomsAreCheckedOut(long bookingId) {
		return getReservationsWithStatus(ReservationStatus.CHECKED_OUT, bookingId).size() == bookingToReservations.get(bookingId).size();
	}
	
	public int nbrOfCheckedOutReservations(long bookingId) {
		return getReservationsWithStatus(ReservationStatus.CHECKED_OUT, bookingId).size();
	}
	
	public boolean shouldUpdateReservation(long bookingId, long reservationId, String status) {
		if (! reservationBelongsToBooking(bookingId, reservationId)) {
			return false;
		}
		
		ReservationStatus checkStatus = ReservationStatus.valueOf(status);
		Reservation reservation = getReservationWithId(bookingToReservations.get(bookingId), reservationId);
		
		return reservation.getStatus() == checkStatus;
	}
	
	private Room getRoomWithId(long bookingId, long roomId) {
		for (Reservation reservation : bookingToReservations.get(bookingId)) {
			if (reservation.getRoom().getId() == roomId) { // @todo NPE -- are rooms being set correctly?
				return reservation.getRoom();
			}
		}
		return null;
	}
	
	public boolean shouldUpdateRoom(long bookingId, long roomId, String status) {
		if (! roomBelongsToBooking(bookingId, roomId)) {
			return false;
		}
		
		RoomStatus checkStatus = RoomStatus.valueOf(status);
		Room room = getRoomWithId(bookingId, roomId);
		return room != null && room.getStatus() == checkStatus;
	}
	
	public boolean canPayForLastRoom(long bookingId) {
		List<Reservation> reservations = bookingToReservations.get(bookingId);
		return reservations.size() == 1;// && reservations.get(0).getStatus() == ReservationStatus.CHECKED_OUT;
	}
}
