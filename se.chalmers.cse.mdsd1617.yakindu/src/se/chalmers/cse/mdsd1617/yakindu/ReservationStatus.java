package se.chalmers.cse.mdsd1617.yakindu;

enum ReservationStatus {
	CREATED(RoomStatus.FREE),
	CONFIRMED(RoomStatus.ASSIGNED),
	CHECKED_IN(RoomStatus.OCCUPIED),
	CHECKED_OUT(RoomStatus.FREE);
	
	private RoomStatus roomStatus;
	
	ReservationStatus(RoomStatus roomStatus) {
		this.roomStatus = roomStatus;
	}
	
	public RoomStatus getRoomStatus() {
		return roomStatus;
	}
	
}
