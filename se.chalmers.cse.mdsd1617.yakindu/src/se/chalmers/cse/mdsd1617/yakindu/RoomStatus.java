package se.chalmers.cse.mdsd1617.yakindu;

enum RoomStatus {
	FREE, ASSIGNED, OCCUPIED;
}
