package se.chalmers.cse.mdsd1617.yakindu;

class Room {
	private final long id;
	private RoomStatus status = RoomStatus.FREE;
	
	public Room(long id) {
		this.id = id;
	}
	
	public long getId() {
		return id;
	}
	
	public void setStatus(RoomStatus status) {
		this.status = status;
	}
	
	public RoomStatus getStatus() {
		return status;
	}
}
