/**
 */
package se.chalmers.cse.mdsd1617.group17;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Availability Manager Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getIRoomAvailabilityManagerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomAvailabilityManagerProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false" startdateRequired="true" startdateOrdered="false" endDateRequired="true" endDateOrdered="false"
	 * @generated
	 */
	EList<Room> getOccupiedListForPeriod(Date startdate, Date endDate);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean isRoomOccupied(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean setRoomOccupied(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model ordered="false"
	 * @generated
	 */
	EList<Room> freeRoomForPeriod();

} // IRoomAvailabilityManagerProvides
