/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Period To Room Type Map Entry</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.PeriodToRoomTypeMapEntry#getKey <em>Key</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.PeriodToRoomTypeMapEntry#getValue <em>Value</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getPeriodToRoomTypeMapEntry()
 * @model
 * @generated
 */
public interface PeriodToRoomTypeMapEntry extends EObject {
	/**
	 * Returns the value of the '<em><b>Key</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Key</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Key</em>' reference.
	 * @see #setKey(Period)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getPeriodToRoomTypeMapEntry_Key()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Period getKey();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.PeriodToRoomTypeMapEntry#getKey <em>Key</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Key</em>' reference.
	 * @see #getKey()
	 * @generated
	 */
	void setKey(Period value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getPeriodToRoomTypeMapEntry_Value()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	RoomType getValue();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.PeriodToRoomTypeMapEntry#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(RoomType value);

} // PeriodToRoomTypeMapEntry
