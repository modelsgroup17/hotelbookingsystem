/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see se.chalmers.cse.mdsd1617.group17.Group17Factory
 * @model kind="package"
 * @generated
 */
public interface Group17Package extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "group17";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///se/chalmers/cse/mdsd1617/group17.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "se.chalmers.cse.mdsd1617.group17";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Group17Package eINSTANCE = se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl.init();

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIHotelCustomerProvides()
	 * @generated
	 */
	int IHOTEL_CUSTOMER_PROVIDES = 0;

	/**
	 * The number of structural features of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = 0;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = 1;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = 2;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = 3;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = 4;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = 5;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = 6;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = 7;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = 8;

	/**
	 * The number of operations of the '<em>IHotel Customer Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.FreeRoomTypesDTOImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getFreeRoomTypesDTO()
	 * @generated
	 */
	int FREE_ROOM_TYPES_DTO = 1;

	/**
	 * The feature id for the '<em><b>Room Type Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = 0;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_BEDS = 1;

	/**
	 * The feature id for the '<em><b>Price Per Night</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = 2;

	/**
	 * The feature id for the '<em><b>Num Free Rooms</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = 3;

	/**
	 * The number of structural features of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Free Room Types DTO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FREE_ROOM_TYPES_DTO_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides <em>IBooking Manager Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIBookingManagerProvides()
	 * @generated
	 */
	int IBOOKING_MANAGER_PROVIDES = 2;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl <em>Room Type</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoomType()
	 * @generated
	 */
	int ROOM_TYPE = 3;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl <em>Booking</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.BookingImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getBooking()
	 * @generated
	 */
	int BOOKING = 4;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl <em>Reservation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getReservation()
	 * @generated
	 */
	int RESERVATION = 5;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomImpl <em>Room</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoom()
	 * @generated
	 */
	int ROOM = 7;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.GuestImpl <em>Guest</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.GuestImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getGuest()
	 * @generated
	 */
	int GUEST = 8;

	/**
	 * The number of structural features of the '<em>IBooking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES_FEATURE_COUNT = IHOTEL_CUSTOMER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___CONFIRM_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___CHECK_IN_ROOM__STRING_INT = IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___INITIATE_CHECKOUT__INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Edit ABooking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___EDIT_ABOOKING__INT_ROOMTYPE_DATE_DATE_INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Cancel ABooking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___CANCEL_ABOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___LIST_BOOKINGS = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The operation id for the '<em>Occupied Rooms For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___OCCUPIED_ROOMS_FOR_DATE__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The operation id for the '<em>List Check Ins For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 4;

	/**
	 * The operation id for the '<em>List Check Outs For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 5;

	/**
	 * The operation id for the '<em>Add Extra Costs For Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___ADD_EXTRA_COSTS_FOR_ROOM__INT_INT_STRING_DOUBLE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 6;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___CHECK_IN_BOOKING__INT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 7;

	/**
	 * The operation id for the '<em>List Check Ins For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 8;

	/**
	 * The operation id for the '<em>List Check Outs For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE_DATE = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 9;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES___CLEAR_BOOKINGS = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 10;

	/**
	 * The number of operations of the '<em>IBooking Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IBOOKING_MANAGER_PROVIDES_OPERATION_COUNT = IHOTEL_CUSTOMER_PROVIDES_OPERATION_COUNT + 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NAME = 0;

	/**
	 * The feature id for the '<em><b>Price</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__PRICE = 1;

	/**
	 * The feature id for the '<em><b>Num Beds</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__NUM_BEDS = 2;

	/**
	 * The feature id for the '<em><b>Additional Features</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE__ADDITIONAL_FEATURES = 3;

	/**
	 * The number of structural features of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Room Type</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_TYPE_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Special Request</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__SPECIAL_REQUEST = 0;

	/**
	 * The feature id for the '<em><b>Reservations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__RESERVATIONS = 1;

	/**
	 * The feature id for the '<em><b>Guest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__GUEST = 2;

	/**
	 * The feature id for the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__BOOKING_ID = 3;

	/**
	 * The feature id for the '<em><b>Period</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__PERIOD = 4;

	/**
	 * The feature id for the '<em><b>Total Price Left To Pay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING__TOTAL_PRICE_LEFT_TO_PAY = 5;

	/**
	 * The number of structural features of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_FEATURE_COUNT = 6;

	/**
	 * The number of operations of the '<em>Booking</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__STATUS = 0;

	/**
	 * The feature id for the '<em><b>Period</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__PERIOD = 1;

	/**
	 * The feature id for the '<em><b>Room</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__ROOM = 2;

	/**
	 * The feature id for the '<em><b>Reservation ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__RESERVATION_ID = 3;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__ROOM_TYPE = 4;

	/**
	 * The feature id for the '<em><b>Extra Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__EXTRA_COST = 5;

	/**
	 * The feature id for the '<em><b>Description Of Extra</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION__DESCRIPTION_OF_EXTRA = 6;

	/**
	 * The number of structural features of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_FEATURE_COUNT = 7;

	/**
	 * The number of operations of the '<em>Reservation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESERVATION_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides <em>IRoom Manager Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIRoomManagerProvides()
	 * @generated
	 */
	int IROOM_MANAGER_PROVIDES = 9;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIHotelStartupProvides()
	 * @generated
	 */
	int IHOTEL_STARTUP_PROVIDES = 10;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getBookingManager()
	 * @generated
	 */
	int BOOKING_MANAGER = 11;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl <em>Room Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoomManager()
	 * @generated
	 */
	int ROOM_MANAGER = 13;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl <em>Startup Manager</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getStartupManager()
	 * @generated
	 */
	int STARTUP_MANAGER = 14;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl <em>Period</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getPeriod()
	 * @generated
	 */
	int PERIOD = 6;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD__START = 0;

	/**
	 * The feature id for the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD__END = 1;

	/**
	 * The number of structural features of the '<em>Period</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD_FEATURE_COUNT = 2;

	/**
	 * The operation id for the '<em>Overlaps With</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD___OVERLAPS_WITH__PERIOD = 0;

	/**
	 * The operation id for the '<em>Is Date In Period</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD___IS_DATE_IN_PERIOD__DATE = 1;

	/**
	 * The number of operations of the '<em>Period</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERIOD_OPERATION_COUNT = 2;

	/**
	 * The feature id for the '<em><b>Room Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_TYPE = 0;

	/**
	 * The feature id for the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM__ROOM_NBR = 1;

	/**
	 * The number of structural features of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>Room</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_OPERATION_COUNT = 0;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUEST__LAST_NAME = 0;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUEST__FIRST_NAME = 1;

	/**
	 * The feature id for the '<em><b>Ss Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUEST__SS_NUM = 2;

	/**
	 * The number of structural features of the '<em>Guest</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUEST_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Guest</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int GUEST_OPERATION_COUNT = 0;

	/**
	 * The number of structural features of the '<em>IRoom Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = 0;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = 1;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___REMOVE_ROOM_TYPE__ROOMTYPE = 2;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___ADD_ROOM__INT_STRING = 3;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = 4;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___REMOVE_ROOM__INT = 5;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___BLOCK_ROOM__INT = 6;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___UNBLOCK_ROOM__INT = 7;

	/**
	 * The operation id for the '<em>Is Room Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___IS_ROOM_BLOCKED__INT = 8;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___GET_ROOM_TYPES = 9;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES___CLEAR_ROOMS = 10;

	/**
	 * The number of operations of the '<em>IRoom Manager Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_MANAGER_PROVIDES_OPERATION_COUNT = 11;

	/**
	 * The number of structural features of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES___STARTUP__INT = 0;

	/**
	 * The number of operations of the '<em>IHotel Startup Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT = 1;

	/**
	 * The feature id for the '<em><b>Iroomavailabilitymanagerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES = IBOOKING_MANAGER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER__BOOKINGS = IBOOKING_MANAGER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_FEATURE_COUNT = IBOOKING_MANAGER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Initiate Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING = IBOOKING_MANAGER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING;

	/**
	 * The operation id for the '<em>Get Free Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___GET_FREE_ROOMS__INT_STRING_STRING = IBOOKING_MANAGER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Add Room To Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__STRING_INT = IBOOKING_MANAGER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT;

	/**
	 * The operation id for the '<em>Confirm Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CONFIRM_BOOKING__INT = IBOOKING_MANAGER_PROVIDES___CONFIRM_BOOKING__INT;

	/**
	 * The operation id for the '<em>Pay During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = IBOOKING_MANAGER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Check In Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT = IBOOKING_MANAGER_PROVIDES___CHECK_IN_ROOM__STRING_INT;

	/**
	 * The operation id for the '<em>Pay Room During Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = IBOOKING_MANAGER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING;

	/**
	 * The operation id for the '<em>Initiate Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_CHECKOUT__INT = IBOOKING_MANAGER_PROVIDES___INITIATE_CHECKOUT__INT;

	/**
	 * The operation id for the '<em>Initiate Room Checkout</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___INITIATE_ROOM_CHECKOUT__INT_INT = IBOOKING_MANAGER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT;

	/**
	 * The operation id for the '<em>Edit ABooking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___EDIT_ABOOKING__INT_ROOMTYPE_DATE_DATE_INT = IBOOKING_MANAGER_PROVIDES___EDIT_ABOOKING__INT_ROOMTYPE_DATE_DATE_INT;

	/**
	 * The operation id for the '<em>Cancel ABooking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CANCEL_ABOOKING__INT = IBOOKING_MANAGER_PROVIDES___CANCEL_ABOOKING__INT;

	/**
	 * The operation id for the '<em>List Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_BOOKINGS = IBOOKING_MANAGER_PROVIDES___LIST_BOOKINGS;

	/**
	 * The operation id for the '<em>Occupied Rooms For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___OCCUPIED_ROOMS_FOR_DATE__DATE = IBOOKING_MANAGER_PROVIDES___OCCUPIED_ROOMS_FOR_DATE__DATE;

	/**
	 * The operation id for the '<em>List Check Ins For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_INS_FOR_DATE__DATE = IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE;

	/**
	 * The operation id for the '<em>List Check Outs For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_OUTS_FOR_DATE__DATE = IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE;

	/**
	 * The operation id for the '<em>Add Extra Costs For Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___ADD_EXTRA_COSTS_FOR_ROOM__INT_INT_STRING_DOUBLE = IBOOKING_MANAGER_PROVIDES___ADD_EXTRA_COSTS_FOR_ROOM__INT_INT_STRING_DOUBLE;

	/**
	 * The operation id for the '<em>Check In Booking</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CHECK_IN_BOOKING__INT = IBOOKING_MANAGER_PROVIDES___CHECK_IN_BOOKING__INT;

	/**
	 * The operation id for the '<em>List Check Ins For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_INS_FOR_DATE__DATE_DATE = IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE_DATE;

	/**
	 * The operation id for the '<em>List Check Outs For Date</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___LIST_CHECK_OUTS_FOR_DATE__DATE_DATE = IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE_DATE;

	/**
	 * The operation id for the '<em>Clear Bookings</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER___CLEAR_BOOKINGS = IBOOKING_MANAGER_PROVIDES___CLEAR_BOOKINGS;

	/**
	 * The number of operations of the '<em>Booking Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BOOKING_MANAGER_OPERATION_COUNT = IBOOKING_MANAGER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides <em>IRoom Access Provides</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIRoomAccessProvides()
	 * @generated
	 */
	int IROOM_ACCESS_PROVIDES = 12;

	/**
	 * The number of structural features of the '<em>IRoom Access Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ACCESS_PROVIDES_FEATURE_COUNT = 0;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ACCESS_PROVIDES___GET_ALL_ROOMS = 0;

	/**
	 * The operation id for the '<em>Is Room Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ACCESS_PROVIDES___IS_ROOM_BLOCKED__ROOM = 1;

	/**
	 * The operation id for the '<em>Get All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ACCESS_PROVIDES___GET_ALL_ROOM_TYPES = 2;

	/**
	 * The number of operations of the '<em>IRoom Access Provides</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IROOM_ACCESS_PROVIDES_OPERATION_COUNT = 3;

	/**
	 * The feature id for the '<em><b>Room Types</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOM_TYPES = IROOM_MANAGER_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Blocked Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__BLOCKED_ROOMS = IROOM_MANAGER_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Rooms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER__ROOMS = IROOM_MANAGER_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_FEATURE_COUNT = IROOM_MANAGER_PROVIDES_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>Add Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = IROOM_MANAGER_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Update Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = IROOM_MANAGER_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room Type</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM_TYPE__ROOMTYPE = IROOM_MANAGER_PROVIDES___REMOVE_ROOM_TYPE__ROOMTYPE;

	/**
	 * The operation id for the '<em>Add Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___ADD_ROOM__INT_STRING = IROOM_MANAGER_PROVIDES___ADD_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Change Room Type Of Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = IROOM_MANAGER_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING;

	/**
	 * The operation id for the '<em>Remove Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___REMOVE_ROOM__INT = IROOM_MANAGER_PROVIDES___REMOVE_ROOM__INT;

	/**
	 * The operation id for the '<em>Block Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___BLOCK_ROOM__INT = IROOM_MANAGER_PROVIDES___BLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Unblock Room</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___UNBLOCK_ROOM__INT = IROOM_MANAGER_PROVIDES___UNBLOCK_ROOM__INT;

	/**
	 * The operation id for the '<em>Is Room Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___IS_ROOM_BLOCKED__INT = IROOM_MANAGER_PROVIDES___IS_ROOM_BLOCKED__INT;

	/**
	 * The operation id for the '<em>Get Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ROOM_TYPES = IROOM_MANAGER_PROVIDES___GET_ROOM_TYPES;

	/**
	 * The operation id for the '<em>Clear Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___CLEAR_ROOMS = IROOM_MANAGER_PROVIDES___CLEAR_ROOMS;

	/**
	 * The operation id for the '<em>Get All Rooms</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ALL_ROOMS = IROOM_MANAGER_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Is Room Blocked</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___IS_ROOM_BLOCKED__ROOM = IROOM_MANAGER_PROVIDES_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Get All Room Types</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER___GET_ALL_ROOM_TYPES = IROOM_MANAGER_PROVIDES_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Room Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROOM_MANAGER_OPERATION_COUNT = IROOM_MANAGER_PROVIDES_OPERATION_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Roommanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STARTUP_MANAGER__ROOMMANAGER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bookingmanager</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STARTUP_MANAGER__BOOKINGMANAGER = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Startup Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STARTUP_MANAGER_FEATURE_COUNT = IHOTEL_STARTUP_PROVIDES_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>Startup</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STARTUP_MANAGER___STARTUP__INT = IHOTEL_STARTUP_PROVIDES___STARTUP__INT;

	/**
	 * The number of operations of the '<em>Startup Manager</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STARTUP_MANAGER_OPERATION_COUNT = IHOTEL_STARTUP_PROVIDES_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link se.chalmers.cse.mdsd1617.group17.ReservationStatus <em>Reservation Status</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.ReservationStatus
	 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getReservationStatus()
	 * @generated
	 */
	int RESERVATION_STATUS = 15;


	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides <em>IHotel Customer Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Customer Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides
	 * @generated
	 */
	EClass getIHotelCustomerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Free Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__GetFreeRooms__int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room To Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__AddRoomToBooking__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Confirm Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#confirmBooking(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__ConfirmBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__CheckInRoom__String_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Pay Room During Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateCheckout__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Initiate Room Checkout</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated
	 */
	EOperation getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO <em>Free Room Types DTO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Free Room Types DTO</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO
	 * @generated
	 */
	EClass getFreeRoomTypesDTO();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getRoomTypeDescription <em>Room Type Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Type Description</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getRoomTypeDescription()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_RoomTypeDescription();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getNumBeds()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getPricePerNight <em>Price Per Night</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price Per Night</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getPricePerNight()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_PricePerNight();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getNumFreeRooms <em>Num Free Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Free Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO#getNumFreeRooms()
	 * @see #getFreeRoomTypesDTO()
	 * @generated
	 */
	EAttribute getFreeRoomTypesDTO_NumFreeRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides <em>IBooking Manager Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IBooking Manager Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides
	 * @generated
	 */
	EClass getIBookingManagerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#editABooking(int, se.chalmers.cse.mdsd1617.group17.RoomType, java.util.Date, java.util.Date, int) <em>Edit ABooking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Edit ABooking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#editABooking(int, se.chalmers.cse.mdsd1617.group17.RoomType, java.util.Date, java.util.Date, int)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__EditABooking__int_RoomType_Date_Date_int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#cancelABooking(int) <em>Cancel ABooking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Cancel ABooking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#cancelABooking(int)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__CancelABooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listBookings() <em>List Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listBookings()
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ListBookings();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#occupiedRoomsForDate(java.util.Date) <em>Occupied Rooms For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Occupied Rooms For Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#occupiedRoomsForDate(java.util.Date)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__OccupiedRoomsForDate__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date) <em>List Check Ins For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins For Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ListCheckInsForDate__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date) <em>List Check Outs For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs For Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ListCheckOutsForDate__Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#addExtraCostsForRoom(int, int, java.lang.String, double) <em>Add Extra Costs For Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Extra Costs For Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#addExtraCostsForRoom(int, int, java.lang.String, double)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__AddExtraCostsForRoom__int_int_String_double();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check In Booking</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#checkInBooking(int)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__CheckInBooking__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date, java.util.Date) <em>List Check Ins For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Ins For Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ListCheckInsForDate__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date, java.util.Date) <em>List Check Outs For Date</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>List Check Outs For Date</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date, java.util.Date)
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ListCheckOutsForDate__Date_Date();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#clearBookings() <em>Clear Bookings</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Bookings</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#clearBookings()
	 * @generated
	 */
	EOperation getIBookingManagerProvides__ClearBookings();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.RoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomType
	 * @generated
	 */
	EClass getRoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.RoomType#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomType#getName()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Name();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.RoomType#getPrice <em>Price</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Price</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomType#getPrice()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_Price();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.RoomType#getNumBeds <em>Num Beds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Num Beds</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomType#getNumBeds()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_NumBeds();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.RoomType#getAdditionalFeatures <em>Additional Features</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Additional Features</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomType#getAdditionalFeatures()
	 * @see #getRoomType()
	 * @generated
	 */
	EAttribute getRoomType_AdditionalFeatures();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.Booking <em>Booking</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking
	 * @generated
	 */
	EClass getBooking();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group17.Booking#getSpecialRequest <em>Special Request</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Special Request</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getSpecialRequest()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_SpecialRequest();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group17.Booking#getReservations <em>Reservations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Reservations</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getReservations()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Reservations();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group17.Booking#getGuest <em>Guest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Guest</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getGuest()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Guest();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Booking#getBookingID <em>Booking ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Booking ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getBookingID()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_BookingID();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group17.Booking#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Period</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getPeriod()
	 * @see #getBooking()
	 * @generated
	 */
	EReference getBooking_Period();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Booking#getTotalPriceLeftToPay <em>Total Price Left To Pay</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Price Left To Pay</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Booking#getTotalPriceLeftToPay()
	 * @see #getBooking()
	 * @generated
	 */
	EAttribute getBooking_TotalPriceLeftToPay();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.Reservation <em>Reservation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Reservation</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation
	 * @generated
	 */
	EClass getReservation();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getStatus()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_Status();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getPeriod <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Period</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getPeriod()
	 * @see #getReservation()
	 * @generated
	 */
	EReference getReservation_Period();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoom <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getRoom()
	 * @see #getReservation()
	 * @generated
	 */
	EReference getReservation_Room();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getReservationID <em>Reservation ID</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Reservation ID</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getReservationID()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_ReservationID();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getRoomType()
	 * @see #getReservation()
	 * @generated
	 */
	EReference getReservation_RoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getExtraCost <em>Extra Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Extra Cost</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getExtraCost()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_ExtraCost();

	/**
	 * Returns the meta object for the attribute list '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getDescriptionOfExtra <em>Description Of Extra</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Description Of Extra</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Reservation#getDescriptionOfExtra()
	 * @see #getReservation()
	 * @generated
	 */
	EAttribute getReservation_DescriptionOfExtra();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.Room <em>Room</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Room
	 * @generated
	 */
	EClass getRoom();

	/**
	 * Returns the meta object for the containment reference '{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomType <em>Room Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Room Type</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Room#getRoomType()
	 * @see #getRoom()
	 * @generated
	 */
	EReference getRoom_RoomType();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomNbr <em>Room Nbr</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Room Nbr</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Room#getRoomNbr()
	 * @see #getRoom()
	 * @generated
	 */
	EAttribute getRoom_RoomNbr();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.Guest <em>Guest</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Guest</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Guest
	 * @generated
	 */
	EClass getGuest();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Guest#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Guest#getLastName()
	 * @see #getGuest()
	 * @generated
	 */
	EAttribute getGuest_LastName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Guest#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Guest#getFirstName()
	 * @see #getGuest()
	 * @generated
	 */
	EAttribute getGuest_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Guest#getSsNum <em>Ss Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Ss Num</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Guest#getSsNum()
	 * @see #getGuest()
	 * @generated
	 */
	EAttribute getGuest_SsNum();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides <em>IRoom Manager Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Manager Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides
	 * @generated
	 */
	EClass getIRoomManagerProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__AddRoomType__String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Update Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__UpdateRoomType__String_String_double_int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoomType(se.chalmers.cse.mdsd1617.group17.RoomType) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room Type</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoomType(se.chalmers.cse.mdsd1617.group17.RoomType)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__RemoveRoomType__RoomType();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Add Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__AddRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#changeRoomTypeOfRoom(int, java.lang.String) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Change Room Type Of Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#changeRoomTypeOfRoom(int, java.lang.String)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__ChangeRoomTypeOfRoom__int_String();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Remove Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoom(int)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__RemoveRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Block Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#blockRoom(int)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__BlockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Unblock Room</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#unblockRoom(int)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__UnblockRoom__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#isRoomBlocked(int) <em>Is Room Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Room Blocked</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#isRoomBlocked(int)
	 * @generated
	 */
	EOperation getIRoomManagerProvides__IsRoomBlocked__int();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#getRoomTypes() <em>Get Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#getRoomTypes()
	 * @generated
	 */
	EOperation getIRoomManagerProvides__GetRoomTypes();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#clearRooms() <em>Clear Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Clear Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#clearRooms()
	 * @generated
	 */
	EOperation getIRoomManagerProvides__ClearRooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides <em>IHotel Startup Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IHotel Startup Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides
	 * @generated
	 */
	EClass getIHotelStartupProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Startup</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides#startup(int)
	 * @generated
	 */
	EOperation getIHotelStartupProvides__Startup__int();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.BookingManager <em>Booking Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Booking Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.BookingManager
	 * @generated
	 */
	EClass getBookingManager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group17.BookingManager#getIroomavailabilitymanagerprovides <em>Iroomavailabilitymanagerprovides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Iroomavailabilitymanagerprovides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.BookingManager#getIroomavailabilitymanagerprovides()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_Iroomavailabilitymanagerprovides();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group17.BookingManager#getBookings <em>Bookings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bookings</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.BookingManager#getBookings()
	 * @see #getBookingManager()
	 * @generated
	 */
	EReference getBookingManager_Bookings();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides <em>IRoom Access Provides</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>IRoom Access Provides</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides
	 * @generated
	 */
	EClass getIRoomAccessProvides();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRooms() <em>Get All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Rooms</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRooms()
	 * @generated
	 */
	EOperation getIRoomAccessProvides__GetAllRooms();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#isRoomBlocked(se.chalmers.cse.mdsd1617.group17.Room) <em>Is Room Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Room Blocked</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#isRoomBlocked(se.chalmers.cse.mdsd1617.group17.Room)
	 * @generated
	 */
	EOperation getIRoomAccessProvides__IsRoomBlocked__Room();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRoomTypes() <em>Get All Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Get All Room Types</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRoomTypes()
	 * @generated
	 */
	EOperation getIRoomAccessProvides__GetAllRoomTypes();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.RoomManager <em>Room Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Room Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomManager
	 * @generated
	 */
	EClass getRoomManager();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getRoomTypes <em>Room Types</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Room Types</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomManager#getRoomTypes()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_RoomTypes();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getBlockedRooms <em>Blocked Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Blocked Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomManager#getBlockedRooms()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_BlockedRooms();

	/**
	 * Returns the meta object for the containment reference list '{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getRooms <em>Rooms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rooms</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.RoomManager#getRooms()
	 * @see #getRoomManager()
	 * @generated
	 */
	EReference getRoomManager_Rooms();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.StartupManager <em>Startup Manager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Startup Manager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.StartupManager
	 * @generated
	 */
	EClass getStartupManager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group17.StartupManager#getBookingmanager <em>Bookingmanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Bookingmanager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.StartupManager#getBookingmanager()
	 * @see #getStartupManager()
	 * @generated
	 */
	EReference getStartupManager_Bookingmanager();

	/**
	 * Returns the meta object for the reference '{@link se.chalmers.cse.mdsd1617.group17.StartupManager#getRoommanager <em>Roommanager</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Roommanager</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.StartupManager#getRoommanager()
	 * @see #getStartupManager()
	 * @generated
	 */
	EReference getStartupManager_Roommanager();

	/**
	 * Returns the meta object for class '{@link se.chalmers.cse.mdsd1617.group17.Period <em>Period</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Period</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Period
	 * @generated
	 */
	EClass getPeriod();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Period#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Period#getStart()
	 * @see #getPeriod()
	 * @generated
	 */
	EAttribute getPeriod_Start();

	/**
	 * Returns the meta object for the attribute '{@link se.chalmers.cse.mdsd1617.group17.Period#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.Period#getEnd()
	 * @see #getPeriod()
	 * @generated
	 */
	EAttribute getPeriod_End();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period) <em>Overlaps With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Overlaps With</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period)
	 * @generated
	 */
	EOperation getPeriod__OverlapsWith__Period();

	/**
	 * Returns the meta object for the '{@link se.chalmers.cse.mdsd1617.group17.Period#isDateInPeriod(java.util.Date) <em>Is Date In Period</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Is Date In Period</em>' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.Period#isDateInPeriod(java.util.Date)
	 * @generated
	 */
	EOperation getPeriod__IsDateInPeriod__Date();

	/**
	 * Returns the meta object for enum '{@link se.chalmers.cse.mdsd1617.group17.ReservationStatus <em>Reservation Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Reservation Status</em>'.
	 * @see se.chalmers.cse.mdsd1617.group17.ReservationStatus
	 * @generated
	 */
	EEnum getReservationStatus();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Group17Factory getGroup17Factory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides <em>IHotel Customer Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIHotelCustomerProvides()
		 * @generated
		 */
		EClass IHOTEL_CUSTOMER_PROVIDES = eINSTANCE.getIHotelCustomerProvides();

		/**
		 * The meta object literal for the '<em><b>Initiate Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_BOOKING__STRING_STRING_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__InitiateBooking__String_String_String_String();

		/**
		 * The meta object literal for the '<em><b>Get Free Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___GET_FREE_ROOMS__INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__GetFreeRooms__int_String_String();

		/**
		 * The meta object literal for the '<em><b>Add Room To Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___ADD_ROOM_TO_BOOKING__STRING_INT = eINSTANCE.getIHotelCustomerProvides__AddRoomToBooking__String_int();

		/**
		 * The meta object literal for the '<em><b>Confirm Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CONFIRM_BOOKING__INT = eINSTANCE.getIHotelCustomerProvides__ConfirmBooking__int();

		/**
		 * The meta object literal for the '<em><b>Pay During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayDuringCheckout__String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Check In Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___CHECK_IN_ROOM__STRING_INT = eINSTANCE.getIHotelCustomerProvides__CheckInRoom__String_int();

		/**
		 * The meta object literal for the '<em><b>Pay Room During Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING = eINSTANCE.getIHotelCustomerProvides__PayRoomDuringCheckout__int_String_String_int_int_String_String();

		/**
		 * The meta object literal for the '<em><b>Initiate Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_CHECKOUT__INT = eINSTANCE.getIHotelCustomerProvides__InitiateCheckout__int();

		/**
		 * The meta object literal for the '<em><b>Initiate Room Checkout</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_CUSTOMER_PROVIDES___INITIATE_ROOM_CHECKOUT__INT_INT = eINSTANCE.getIHotelCustomerProvides__InitiateRoomCheckout__int_int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.FreeRoomTypesDTOImpl <em>Free Room Types DTO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.FreeRoomTypesDTOImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getFreeRoomTypesDTO()
		 * @generated
		 */
		EClass FREE_ROOM_TYPES_DTO = eINSTANCE.getFreeRoomTypesDTO();

		/**
		 * The meta object literal for the '<em><b>Room Type Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__ROOM_TYPE_DESCRIPTION = eINSTANCE.getFreeRoomTypesDTO_RoomTypeDescription();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_BEDS = eINSTANCE.getFreeRoomTypesDTO_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Price Per Night</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__PRICE_PER_NIGHT = eINSTANCE.getFreeRoomTypesDTO_PricePerNight();

		/**
		 * The meta object literal for the '<em><b>Num Free Rooms</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FREE_ROOM_TYPES_DTO__NUM_FREE_ROOMS = eINSTANCE.getFreeRoomTypesDTO_NumFreeRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides <em>IBooking Manager Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIBookingManagerProvides()
		 * @generated
		 */
		EClass IBOOKING_MANAGER_PROVIDES = eINSTANCE.getIBookingManagerProvides();

		/**
		 * The meta object literal for the '<em><b>Edit ABooking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___EDIT_ABOOKING__INT_ROOMTYPE_DATE_DATE_INT = eINSTANCE.getIBookingManagerProvides__EditABooking__int_RoomType_Date_Date_int();

		/**
		 * The meta object literal for the '<em><b>Cancel ABooking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___CANCEL_ABOOKING__INT = eINSTANCE.getIBookingManagerProvides__CancelABooking__int();

		/**
		 * The meta object literal for the '<em><b>List Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___LIST_BOOKINGS = eINSTANCE.getIBookingManagerProvides__ListBookings();

		/**
		 * The meta object literal for the '<em><b>Occupied Rooms For Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___OCCUPIED_ROOMS_FOR_DATE__DATE = eINSTANCE.getIBookingManagerProvides__OccupiedRoomsForDate__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Ins For Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE = eINSTANCE.getIBookingManagerProvides__ListCheckInsForDate__Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs For Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE = eINSTANCE.getIBookingManagerProvides__ListCheckOutsForDate__Date();

		/**
		 * The meta object literal for the '<em><b>Add Extra Costs For Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___ADD_EXTRA_COSTS_FOR_ROOM__INT_INT_STRING_DOUBLE = eINSTANCE.getIBookingManagerProvides__AddExtraCostsForRoom__int_int_String_double();

		/**
		 * The meta object literal for the '<em><b>Check In Booking</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___CHECK_IN_BOOKING__INT = eINSTANCE.getIBookingManagerProvides__CheckInBooking__int();

		/**
		 * The meta object literal for the '<em><b>List Check Ins For Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___LIST_CHECK_INS_FOR_DATE__DATE_DATE = eINSTANCE.getIBookingManagerProvides__ListCheckInsForDate__Date_Date();

		/**
		 * The meta object literal for the '<em><b>List Check Outs For Date</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___LIST_CHECK_OUTS_FOR_DATE__DATE_DATE = eINSTANCE.getIBookingManagerProvides__ListCheckOutsForDate__Date_Date();

		/**
		 * The meta object literal for the '<em><b>Clear Bookings</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IBOOKING_MANAGER_PROVIDES___CLEAR_BOOKINGS = eINSTANCE.getIBookingManagerProvides__ClearBookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl <em>Room Type</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoomType()
		 * @generated
		 */
		EClass ROOM_TYPE = eINSTANCE.getRoomType();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NAME = eINSTANCE.getRoomType_Name();

		/**
		 * The meta object literal for the '<em><b>Price</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__PRICE = eINSTANCE.getRoomType_Price();

		/**
		 * The meta object literal for the '<em><b>Num Beds</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__NUM_BEDS = eINSTANCE.getRoomType_NumBeds();

		/**
		 * The meta object literal for the '<em><b>Additional Features</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM_TYPE__ADDITIONAL_FEATURES = eINSTANCE.getRoomType_AdditionalFeatures();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl <em>Booking</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.BookingImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getBooking()
		 * @generated
		 */
		EClass BOOKING = eINSTANCE.getBooking();

		/**
		 * The meta object literal for the '<em><b>Special Request</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__SPECIAL_REQUEST = eINSTANCE.getBooking_SpecialRequest();

		/**
		 * The meta object literal for the '<em><b>Reservations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__RESERVATIONS = eINSTANCE.getBooking_Reservations();

		/**
		 * The meta object literal for the '<em><b>Guest</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__GUEST = eINSTANCE.getBooking_Guest();

		/**
		 * The meta object literal for the '<em><b>Booking ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__BOOKING_ID = eINSTANCE.getBooking_BookingID();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING__PERIOD = eINSTANCE.getBooking_Period();

		/**
		 * The meta object literal for the '<em><b>Total Price Left To Pay</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BOOKING__TOTAL_PRICE_LEFT_TO_PAY = eINSTANCE.getBooking_TotalPriceLeftToPay();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl <em>Reservation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getReservation()
		 * @generated
		 */
		EClass RESERVATION = eINSTANCE.getReservation();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__STATUS = eINSTANCE.getReservation_Status();

		/**
		 * The meta object literal for the '<em><b>Period</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION__PERIOD = eINSTANCE.getReservation_Period();

		/**
		 * The meta object literal for the '<em><b>Room</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION__ROOM = eINSTANCE.getReservation_Room();

		/**
		 * The meta object literal for the '<em><b>Reservation ID</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__RESERVATION_ID = eINSTANCE.getReservation_ReservationID();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RESERVATION__ROOM_TYPE = eINSTANCE.getReservation_RoomType();

		/**
		 * The meta object literal for the '<em><b>Extra Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__EXTRA_COST = eINSTANCE.getReservation_ExtraCost();

		/**
		 * The meta object literal for the '<em><b>Description Of Extra</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESERVATION__DESCRIPTION_OF_EXTRA = eINSTANCE.getReservation_DescriptionOfExtra();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomImpl <em>Room</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoom()
		 * @generated
		 */
		EClass ROOM = eINSTANCE.getRoom();

		/**
		 * The meta object literal for the '<em><b>Room Type</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM__ROOM_TYPE = eINSTANCE.getRoom_RoomType();

		/**
		 * The meta object literal for the '<em><b>Room Nbr</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROOM__ROOM_NBR = eINSTANCE.getRoom_RoomNbr();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.GuestImpl <em>Guest</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.GuestImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getGuest()
		 * @generated
		 */
		EClass GUEST = eINSTANCE.getGuest();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUEST__LAST_NAME = eINSTANCE.getGuest_LastName();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUEST__FIRST_NAME = eINSTANCE.getGuest_FirstName();

		/**
		 * The meta object literal for the '<em><b>Ss Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute GUEST__SS_NUM = eINSTANCE.getGuest_SsNum();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides <em>IRoom Manager Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIRoomManagerProvides()
		 * @generated
		 */
		EClass IROOM_MANAGER_PROVIDES = eINSTANCE.getIRoomManagerProvides();

		/**
		 * The meta object literal for the '<em><b>Add Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomManagerProvides__AddRoomType__String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Update Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING = eINSTANCE.getIRoomManagerProvides__UpdateRoomType__String_String_double_int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room Type</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___REMOVE_ROOM_TYPE__ROOMTYPE = eINSTANCE.getIRoomManagerProvides__RemoveRoomType__RoomType();

		/**
		 * The meta object literal for the '<em><b>Add Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___ADD_ROOM__INT_STRING = eINSTANCE.getIRoomManagerProvides__AddRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Change Room Type Of Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING = eINSTANCE.getIRoomManagerProvides__ChangeRoomTypeOfRoom__int_String();

		/**
		 * The meta object literal for the '<em><b>Remove Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___REMOVE_ROOM__INT = eINSTANCE.getIRoomManagerProvides__RemoveRoom__int();

		/**
		 * The meta object literal for the '<em><b>Block Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___BLOCK_ROOM__INT = eINSTANCE.getIRoomManagerProvides__BlockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Unblock Room</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___UNBLOCK_ROOM__INT = eINSTANCE.getIRoomManagerProvides__UnblockRoom__int();

		/**
		 * The meta object literal for the '<em><b>Is Room Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___IS_ROOM_BLOCKED__INT = eINSTANCE.getIRoomManagerProvides__IsRoomBlocked__int();

		/**
		 * The meta object literal for the '<em><b>Get Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___GET_ROOM_TYPES = eINSTANCE.getIRoomManagerProvides__GetRoomTypes();

		/**
		 * The meta object literal for the '<em><b>Clear Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_MANAGER_PROVIDES___CLEAR_ROOMS = eINSTANCE.getIRoomManagerProvides__ClearRooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides <em>IHotel Startup Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIHotelStartupProvides()
		 * @generated
		 */
		EClass IHOTEL_STARTUP_PROVIDES = eINSTANCE.getIHotelStartupProvides();

		/**
		 * The meta object literal for the '<em><b>Startup</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IHOTEL_STARTUP_PROVIDES___STARTUP__INT = eINSTANCE.getIHotelStartupProvides__Startup__int();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl <em>Booking Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getBookingManager()
		 * @generated
		 */
		EClass BOOKING_MANAGER = eINSTANCE.getBookingManager();

		/**
		 * The meta object literal for the '<em><b>Iroomavailabilitymanagerprovides</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES = eINSTANCE.getBookingManager_Iroomavailabilitymanagerprovides();

		/**
		 * The meta object literal for the '<em><b>Bookings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BOOKING_MANAGER__BOOKINGS = eINSTANCE.getBookingManager_Bookings();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides <em>IRoom Access Provides</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getIRoomAccessProvides()
		 * @generated
		 */
		EClass IROOM_ACCESS_PROVIDES = eINSTANCE.getIRoomAccessProvides();

		/**
		 * The meta object literal for the '<em><b>Get All Rooms</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ACCESS_PROVIDES___GET_ALL_ROOMS = eINSTANCE.getIRoomAccessProvides__GetAllRooms();

		/**
		 * The meta object literal for the '<em><b>Is Room Blocked</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ACCESS_PROVIDES___IS_ROOM_BLOCKED__ROOM = eINSTANCE.getIRoomAccessProvides__IsRoomBlocked__Room();

		/**
		 * The meta object literal for the '<em><b>Get All Room Types</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation IROOM_ACCESS_PROVIDES___GET_ALL_ROOM_TYPES = eINSTANCE.getIRoomAccessProvides__GetAllRoomTypes();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl <em>Room Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getRoomManager()
		 * @generated
		 */
		EClass ROOM_MANAGER = eINSTANCE.getRoomManager();

		/**
		 * The meta object literal for the '<em><b>Room Types</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOM_TYPES = eINSTANCE.getRoomManager_RoomTypes();

		/**
		 * The meta object literal for the '<em><b>Blocked Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__BLOCKED_ROOMS = eINSTANCE.getRoomManager_BlockedRooms();

		/**
		 * The meta object literal for the '<em><b>Rooms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROOM_MANAGER__ROOMS = eINSTANCE.getRoomManager_Rooms();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl <em>Startup Manager</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getStartupManager()
		 * @generated
		 */
		EClass STARTUP_MANAGER = eINSTANCE.getStartupManager();

		/**
		 * The meta object literal for the '<em><b>Bookingmanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STARTUP_MANAGER__BOOKINGMANAGER = eINSTANCE.getStartupManager_Bookingmanager();

		/**
		 * The meta object literal for the '<em><b>Roommanager</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference STARTUP_MANAGER__ROOMMANAGER = eINSTANCE.getStartupManager_Roommanager();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl <em>Period</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getPeriod()
		 * @generated
		 */
		EClass PERIOD = eINSTANCE.getPeriod();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIOD__START = eINSTANCE.getPeriod_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERIOD__END = eINSTANCE.getPeriod_End();

		/**
		 * The meta object literal for the '<em><b>Overlaps With</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERIOD___OVERLAPS_WITH__PERIOD = eINSTANCE.getPeriod__OverlapsWith__Period();

		/**
		 * The meta object literal for the '<em><b>Is Date In Period</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERIOD___IS_DATE_IN_PERIOD__DATE = eINSTANCE.getPeriod__IsDateInPeriod__Date();

		/**
		 * The meta object literal for the '{@link se.chalmers.cse.mdsd1617.group17.ReservationStatus <em>Reservation Status</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see se.chalmers.cse.mdsd1617.group17.ReservationStatus
		 * @see se.chalmers.cse.mdsd1617.group17.impl.Group17PackageImpl#getReservationStatus()
		 * @generated
		 */
		EEnum RESERVATION_STATUS = eINSTANCE.getReservationStatus();

	}

} //Group17Package
