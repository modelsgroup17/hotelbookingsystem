/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomNbr <em>Room Nbr</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoom()
 * @model
 * @generated
 */
public interface Room extends EObject {
	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' containment reference.
	 * @see #setRoomType(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoom_RoomType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	RoomType getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomType <em>Room Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' containment reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(RoomType value);

	/**
	 * Returns the value of the '<em><b>Room Nbr</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Nbr</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Nbr</em>' attribute.
	 * @see #setRoomNbr(int)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoom_RoomNbr()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getRoomNbr();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Room#getRoomNbr <em>Room Nbr</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Nbr</em>' attribute.
	 * @see #getRoomNbr()
	 * @generated
	 */
	void setRoomNbr(int value);

} // Room
