/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.BookingManager#getIroomavailabilitymanagerprovides <em>Iroomavailabilitymanagerprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.BookingManager#getBookings <em>Bookings</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBookingManager()
 * @model
 * @generated
 */
public interface BookingManager extends IBookingManagerProvides {
	/**
	 * Returns the value of the '<em><b>Iroomavailabilitymanagerprovides</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iroomavailabilitymanagerprovides</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iroomavailabilitymanagerprovides</em>' reference.
	 * @see #setIroomavailabilitymanagerprovides(IRoomAccessProvides)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBookingManager_Iroomavailabilitymanagerprovides()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	IRoomAccessProvides getIroomavailabilitymanagerprovides();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.BookingManager#getIroomavailabilitymanagerprovides <em>Iroomavailabilitymanagerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iroomavailabilitymanagerprovides</em>' reference.
	 * @see #getIroomavailabilitymanagerprovides()
	 * @generated
	 */
	void setIroomavailabilitymanagerprovides(IRoomAccessProvides value);

	/**
	 * Returns the value of the '<em><b>Bookings</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group17.Booking}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bookings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bookings</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBookingManager_Bookings()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Booking> getBookings();

} // BookingManager
