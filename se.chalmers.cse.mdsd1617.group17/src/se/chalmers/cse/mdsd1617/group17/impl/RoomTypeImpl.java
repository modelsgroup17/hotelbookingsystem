/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Type</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl#getName <em>Name</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl#getPrice <em>Price</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl#getNumBeds <em>Num Beds</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl#getAdditionalFeatures <em>Additional Features</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomTypeImpl extends MinimalEObjectImpl.Container implements RoomType {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected static final double PRICE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getPrice() <em>Price</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPrice()
	 * @generated
	 * @ordered
	 */
	protected double price = PRICE_EDEFAULT;

	/**
	 * The default value of the '{@link #getNumBeds() <em>Num Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumBeds()
	 * @generated
	 * @ordered
	 */
	protected static final int NUM_BEDS_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getNumBeds() <em>Num Beds</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getNumBeds()
	 * @generated
	 * @ordered
	 */
	protected int numBeds = NUM_BEDS_EDEFAULT;

	/**
	 * The default value of the '{@link #getAdditionalFeatures() <em>Additional Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalFeatures()
	 * @generated
	 * @ordered
	 */
	protected static final String ADDITIONAL_FEATURES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getAdditionalFeatures() <em>Additional Features</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAdditionalFeatures()
	 * @generated
	 * @ordered
	 */
	protected String additionalFeatures = ADDITIONAL_FEATURES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.ROOM_TYPE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setName(String newName) {
		name = newName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getPrice() {
		return price;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPrice(double newPrice) {
		price = newPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getNumBeds() {
		return numBeds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setNumBeds(int newNumBeds) {
		numBeds = newNumBeds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getAdditionalFeatures() {
		return additionalFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setAdditionalFeatures(String newAdditionalFeatures) {
		additionalFeatures = newAdditionalFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.ROOM_TYPE__NAME:
				return getName();
			case Group17Package.ROOM_TYPE__PRICE:
				return getPrice();
			case Group17Package.ROOM_TYPE__NUM_BEDS:
				return getNumBeds();
			case Group17Package.ROOM_TYPE__ADDITIONAL_FEATURES:
				return getAdditionalFeatures();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.ROOM_TYPE__NAME:
				setName((String)newValue);
				return;
			case Group17Package.ROOM_TYPE__PRICE:
				setPrice((Double)newValue);
				return;
			case Group17Package.ROOM_TYPE__NUM_BEDS:
				setNumBeds((Integer)newValue);
				return;
			case Group17Package.ROOM_TYPE__ADDITIONAL_FEATURES:
				setAdditionalFeatures((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.ROOM_TYPE__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Group17Package.ROOM_TYPE__PRICE:
				setPrice(PRICE_EDEFAULT);
				return;
			case Group17Package.ROOM_TYPE__NUM_BEDS:
				setNumBeds(NUM_BEDS_EDEFAULT);
				return;
			case Group17Package.ROOM_TYPE__ADDITIONAL_FEATURES:
				setAdditionalFeatures(ADDITIONAL_FEATURES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.ROOM_TYPE__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Group17Package.ROOM_TYPE__PRICE:
				return price != PRICE_EDEFAULT;
			case Group17Package.ROOM_TYPE__NUM_BEDS:
				return numBeds != NUM_BEDS_EDEFAULT;
			case Group17Package.ROOM_TYPE__ADDITIONAL_FEATURES:
				return ADDITIONAL_FEATURES_EDEFAULT == null ? additionalFeatures != null : !ADDITIONAL_FEATURES_EDEFAULT.equals(additionalFeatures);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", price: ");
		result.append(price);
		result.append(", numBeds: ");
		result.append(numBeds);
		result.append(", additionalFeatures: ");
		result.append(additionalFeatures);
		result.append(')');
		return result.toString();
	}

} //RoomTypeImpl
