/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group17.BookingManager;
import se.chalmers.cse.mdsd1617.group17.Group17Factory;
import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.RoomManager;
import se.chalmers.cse.mdsd1617.group17.StartupManager;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Startup Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl#getRoommanager <em>Roommanager</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.StartupManagerImpl#getBookingmanager <em>Bookingmanager</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StartupManagerImpl extends MinimalEObjectImpl.Container implements StartupManager {
	/**
	 * The cached value of the '{@link #getRoommanager() <em>Roommanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoommanager()
	 * @generated
	 * @ordered
	 */
	protected RoomManager roommanager;

	/**
	 * The cached value of the '{@link #getBookingmanager() <em>Bookingmanager</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingmanager()
	 * @generated
	 * @ordered
	 */
	protected BookingManager bookingmanager;
	
	private static StartupManager instance;
	
	public static StartupManager getInstance() {
		if (instance == null) {
			instance = new StartupManagerImpl();
		}
		return instance;
	}
	

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public StartupManagerImpl() {
		super();
		bookingmanager = Group17Factory.eINSTANCE.createBookingManager();
		roommanager = Group17Factory.eINSTANCE.createRoomManager();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.STARTUP_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager getBookingmanager() {
		if (bookingmanager != null && bookingmanager.eIsProxy()) {
			InternalEObject oldBookingmanager = (InternalEObject)bookingmanager;
			bookingmanager = (BookingManager)eResolveProxy(oldBookingmanager);
			if (bookingmanager != oldBookingmanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group17Package.STARTUP_MANAGER__BOOKINGMANAGER, oldBookingmanager, bookingmanager));
			}
		}
		return bookingmanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManager basicGetBookingmanager() {
		return bookingmanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBookingmanager(BookingManager newBookingmanager) {
		BookingManager oldBookingmanager = bookingmanager;
		bookingmanager = newBookingmanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group17Package.STARTUP_MANAGER__BOOKINGMANAGER, oldBookingmanager, bookingmanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager getRoommanager() {
		if (roommanager != null && roommanager.eIsProxy()) {
			InternalEObject oldRoommanager = (InternalEObject)roommanager;
			roommanager = (RoomManager)eResolveProxy(oldRoommanager);
			if (roommanager != oldRoommanager) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group17Package.STARTUP_MANAGER__ROOMMANAGER, oldRoommanager, roommanager));
			}
		}
		return roommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManager basicGetRoommanager() {
		return roommanager;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRoommanager(RoomManager newRoommanager) {
		RoomManager oldRoommanager = roommanager;
		roommanager = newRoommanager;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group17Package.STARTUP_MANAGER__ROOMMANAGER, oldRoommanager, roommanager));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void startup(int numRooms) {
		try {
			
			bookingmanager.clearBookings();
			roommanager.clearRooms();
			roommanager.addRoomType("default", 1200, 2, "");
		    
			for (int i = 0; i<numRooms;i++) {
				roommanager.addRoom(i, "default");
			}
		
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.STARTUP_MANAGER__ROOMMANAGER:
				if (resolve) return getRoommanager();
				return basicGetRoommanager();
			case Group17Package.STARTUP_MANAGER__BOOKINGMANAGER:
				if (resolve) return getBookingmanager();
				return basicGetBookingmanager();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.STARTUP_MANAGER__ROOMMANAGER:
				setRoommanager((RoomManager)newValue);
				return;
			case Group17Package.STARTUP_MANAGER__BOOKINGMANAGER:
				setBookingmanager((BookingManager)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.STARTUP_MANAGER__ROOMMANAGER:
				setRoommanager((RoomManager)null);
				return;
			case Group17Package.STARTUP_MANAGER__BOOKINGMANAGER:
				setBookingmanager((BookingManager)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.STARTUP_MANAGER__ROOMMANAGER:
				return roommanager != null;
			case Group17Package.STARTUP_MANAGER__BOOKINGMANAGER:
				return bookingmanager != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group17Package.STARTUP_MANAGER___STARTUP__INT:
				startup((Integer)arguments.get(0));
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //StartupManagerImpl
