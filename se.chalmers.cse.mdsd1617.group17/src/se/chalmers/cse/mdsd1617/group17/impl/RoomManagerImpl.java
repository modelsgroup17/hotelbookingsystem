/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;
import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import javax.xml.soap.SOAPException;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires;
import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomManager;
import se.chalmers.cse.mdsd1617.group17.RoomType;


/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl#getRoomTypes <em>Room Types</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl#getBlockedRooms <em>Blocked Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RoomManagerImpl extends MinimalEObjectImpl.Container implements RoomManager {
	
	/**
	 * The cached value of the '{@link #getRoomTypes() <em>Room Types</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomTypes()
	 * @generated
	 * @ordered
	 */
	protected EList<RoomType> roomTypes;

	/**
	 * The cached value of the '{@link #getBlockedRooms() <em>Blocked Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBlockedRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> blockedRooms;

	/**
	 * The cached value of the '{@link #getRooms() <em>Rooms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRooms()
	 * @generated
	 * @ordered
	 */
	protected EList<Room> rooms;

	private AdministratorRequires bankingAdmin;
	
	private static RoomManagerImpl instance;
	
	public static RoomManagerImpl getInstance() {
		if (instance == null) {
			instance = new RoomManagerImpl();
		}
		return instance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public RoomManagerImpl() {
		super();
		rooms = new BasicEList<Room>();
		roomTypes = new BasicEList<RoomType>();
		blockedRooms = new BasicEList<Room>();
		try {
			bankingAdmin = se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public AdministratorRequires getBankingAdmin(){
		
		return bankingAdmin;
		
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.ROOM_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<RoomType> getRoomTypes() {
		if (roomTypes == null) {
			roomTypes = new EObjectContainmentEList<RoomType>(RoomType.class, this, Group17Package.ROOM_MANAGER__ROOM_TYPES);
		}
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getBlockedRooms() {
		if (blockedRooms == null) {
			blockedRooms = new EObjectContainmentEList<Room>(Room.class, this, Group17Package.ROOM_MANAGER__BLOCKED_ROOMS);
		}
		return blockedRooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Room> getRooms() {
		if (rooms == null) {
			rooms = new EObjectContainmentEList<Room>(Room.class, this, Group17Package.ROOM_MANAGER__ROOMS);
		}
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoomType(String roomTypeName, double price, int numBeds, String additionalFeatures) {
		RoomType roomType = new RoomTypeImpl();
		roomType.setName(roomTypeName);
		roomType.setPrice(price);
		roomType.setNumBeds(numBeds);
		roomType.setAdditionalFeatures(additionalFeatures);
		roomTypes.add(roomType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void updateRoomType(String oldRoomTypeName, String newRoomTypeName, double price, int numBeds, String additionalFeatures) {
		RoomType roomType = getRoomTypeByName(oldRoomTypeName);
		roomType.setName(newRoomTypeName);
		roomType.setPrice(price);
		roomType.setNumBeds(numBeds);
		roomType.setAdditionalFeatures(additionalFeatures);
	}
	
	private RoomType getRoomTypeByName(String name) {
		for (RoomType type : roomTypes) {
			if (type.getName().equals(name)) {
				return type;
			}
		}
		
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoomType(RoomType romeType) {
		roomTypes.remove(romeType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addRoom(int roomNbr, String roomTypeName) {
		if (roomNbr < 0) {
			return;
		}
		
		RoomType roomType = getRoomTypeByName(roomTypeName);
		Room room = new RoomImpl();
		room.setRoomNbr(roomNbr);
		room.setRoomType(roomType);
		rooms.add(room);
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void changeRoomTypeOfRoom(int roomNbr, String roomTypeName) {
		Room room = getRoomByNumber(roomNbr);
		RoomType type = getRoomTypeByName(roomTypeName);		
		room.setRoomType(type);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void removeRoom(int roomNbr) {
		Room room = getRoomByNumber(roomNbr);
		rooms.remove(room);
	}
	
	private Room getRoomByNumber(int roomNbr) {
		for (Room r : rooms) {
			if (r.getRoomNbr()== roomNbr) {
				return r;
			}
		}
		
		throw new IllegalArgumentException("Invalid room number");
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void blockRoom(int roomNbr) {
		blockedRooms.add(getRoomByNumber(roomNbr));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void unblockRoom(int roomNbr) {
		if(isRoomBlocked(roomNbr)==true){
			blockedRooms.remove(getRoomByNumber(roomNbr));
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isRoomBlocked(int roomNbr) {
		return blockedRooms.contains(getRoomByNumber(roomNbr)); // if room number is in the list of blocked rooms, then return true
	
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearRooms() {
		rooms.clear();
		roomTypes.clear();
		blockedRooms.clear();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Room> getAllRooms() {
		return rooms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isRoomBlocked(Room room) {
		return blockedRooms.contains(room);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<RoomType> getAllRoomTypes() {
		return roomTypes;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Group17Package.ROOM_MANAGER__ROOM_TYPES:
				return ((InternalEList<?>)getRoomTypes()).basicRemove(otherEnd, msgs);
			case Group17Package.ROOM_MANAGER__BLOCKED_ROOMS:
				return ((InternalEList<?>)getBlockedRooms()).basicRemove(otherEnd, msgs);
			case Group17Package.ROOM_MANAGER__ROOMS:
				return ((InternalEList<?>)getRooms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.ROOM_MANAGER__ROOM_TYPES:
				return getRoomTypes();
			case Group17Package.ROOM_MANAGER__BLOCKED_ROOMS:
				return getBlockedRooms();
			case Group17Package.ROOM_MANAGER__ROOMS:
				return getRooms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.ROOM_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				getRoomTypes().addAll((Collection<? extends RoomType>)newValue);
				return;
			case Group17Package.ROOM_MANAGER__BLOCKED_ROOMS:
				getBlockedRooms().clear();
				getBlockedRooms().addAll((Collection<? extends Room>)newValue);
				return;
			case Group17Package.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				getRooms().addAll((Collection<? extends Room>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.ROOM_MANAGER__ROOM_TYPES:
				getRoomTypes().clear();
				return;
			case Group17Package.ROOM_MANAGER__BLOCKED_ROOMS:
				getBlockedRooms().clear();
				return;
			case Group17Package.ROOM_MANAGER__ROOMS:
				getRooms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.ROOM_MANAGER__ROOM_TYPES:
				return roomTypes != null && !roomTypes.isEmpty();
			case Group17Package.ROOM_MANAGER__BLOCKED_ROOMS:
				return blockedRooms != null && !blockedRooms.isEmpty();
			case Group17Package.ROOM_MANAGER__ROOMS:
				return rooms != null && !rooms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public int eDerivedOperationID(int baseOperationID, Class<?> baseClass) {
		if (baseClass == IRoomAccessProvides.class) {
			switch (baseOperationID) {
				case Group17Package.IROOM_ACCESS_PROVIDES___GET_ALL_ROOMS: return Group17Package.ROOM_MANAGER___GET_ALL_ROOMS;
				case Group17Package.IROOM_ACCESS_PROVIDES___IS_ROOM_BLOCKED__ROOM: return Group17Package.ROOM_MANAGER___IS_ROOM_BLOCKED__ROOM;
				case Group17Package.IROOM_ACCESS_PROVIDES___GET_ALL_ROOM_TYPES: return Group17Package.ROOM_MANAGER___GET_ALL_ROOM_TYPES;
				default: return -1;
			}
		}
		return super.eDerivedOperationID(baseOperationID, baseClass);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group17Package.ROOM_MANAGER___ADD_ROOM_TYPE__STRING_DOUBLE_INT_STRING:
				addRoomType((String)arguments.get(0), (Double)arguments.get(1), (Integer)arguments.get(2), (String)arguments.get(3));
				return null;
			case Group17Package.ROOM_MANAGER___UPDATE_ROOM_TYPE__STRING_STRING_DOUBLE_INT_STRING:
				updateRoomType((String)arguments.get(0), (String)arguments.get(1), (Double)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4));
				return null;
			case Group17Package.ROOM_MANAGER___REMOVE_ROOM_TYPE__ROOMTYPE:
				removeRoomType((RoomType)arguments.get(0));
				return null;
			case Group17Package.ROOM_MANAGER___ADD_ROOM__INT_STRING:
				addRoom((Integer)arguments.get(0), (String)arguments.get(1));
				return null;
			case Group17Package.ROOM_MANAGER___CHANGE_ROOM_TYPE_OF_ROOM__INT_STRING:
				changeRoomTypeOfRoom((Integer)arguments.get(0), (String)arguments.get(1));
				return null;
			case Group17Package.ROOM_MANAGER___REMOVE_ROOM__INT:
				removeRoom((Integer)arguments.get(0));
				return null;
			case Group17Package.ROOM_MANAGER___BLOCK_ROOM__INT:
				blockRoom((Integer)arguments.get(0));
				return null;
			case Group17Package.ROOM_MANAGER___UNBLOCK_ROOM__INT:
				unblockRoom((Integer)arguments.get(0));
				return null;
			case Group17Package.ROOM_MANAGER___IS_ROOM_BLOCKED__INT:
				return isRoomBlocked((Integer)arguments.get(0));
			case Group17Package.ROOM_MANAGER___CLEAR_ROOMS:
				clearRooms();
				return null;
			case Group17Package.ROOM_MANAGER___GET_ALL_ROOMS:
				return getAllRooms();
			case Group17Package.ROOM_MANAGER___IS_ROOM_BLOCKED__ROOM:
				return isRoomBlocked((Room)arguments.get(0));
			case Group17Package.ROOM_MANAGER___GET_ALL_ROOM_TYPES:
				return getAllRoomTypes();
		}
		return super.eInvoke(operationID, arguments);
	}

} //RoomManagerImpl
