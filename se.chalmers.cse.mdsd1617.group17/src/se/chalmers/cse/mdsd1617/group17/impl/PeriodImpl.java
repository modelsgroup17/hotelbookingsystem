/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.Period;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Period</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl#getStart <em>Start</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl#getEnd <em>End</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PeriodImpl extends MinimalEObjectImpl.Container implements Period {
	/**
	 * The default value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected static final Date START_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStart() <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStart()
	 * @generated
	 * @ordered
	 */
	protected Date start = START_EDEFAULT;

	/**
	 * The default value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected static final Date END_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEnd() <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEnd()
	 * @generated
	 * @ordered
	 */
	protected Date end = END_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PeriodImpl() {
		super();
	}
	
	public PeriodImpl(Date start, Date end) throws ParseException {
		if (start.after(end)) {
			throw new ParseException("start date needs to be after end date.", 0);
		}
		setStart(start);
		setEnd(end);
	}
	
	public PeriodImpl(String start, String end) throws ParseException {
		if (start.contains("-") || end.contains("-")) {
			throw new ParseException("Use date format yyyymmdd.", 0);
		}
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		try {
			Date startDate = df.parse(start);
			Date endDate = df.parse(end);
			
			if (startDate.after(endDate)) {
				throw new ParseException("Start date needs to be before end date.", 0);
			}

			setStart(startDate);
			setEnd(endDate);
		} catch (Exception e) {
			if (e instanceof ParseException) {
				// pass the ParseException to the caller
				throw e;
			}
			e.printStackTrace();
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.PERIOD;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	@Override
	public boolean equals(Object o) {
		if (o instanceof Period) {
			Period p = (Period)o;
			if(p.getStart().equals(this.start) && p.getEnd().equals(this.end)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStart() {
		return start;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setStart(Date newStart) {
		start = newStart;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEnd() {
		return end;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setEnd(Date newEnd) {
		end = newEnd;
	}

	/**
	 * <!-- begin-user-doc -->
	 * Returns whether the specified intervals overlaps.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean overlapsWith(Period period) {
		return period != null && start.getTime() <= period.getEnd().getTime() && end.getTime() >= period.getStart().getTime();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean isDateInPeriod(Date date) {
		return (date.equals(start) || date.after(start)) && (date.equals(end) || date.before(end));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.PERIOD__START:
				return getStart();
			case Group17Package.PERIOD__END:
				return getEnd();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.PERIOD__START:
				setStart((Date)newValue);
				return;
			case Group17Package.PERIOD__END:
				setEnd((Date)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.PERIOD__START:
				setStart(START_EDEFAULT);
				return;
			case Group17Package.PERIOD__END:
				setEnd(END_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.PERIOD__START:
				return START_EDEFAULT == null ? start != null : !START_EDEFAULT.equals(start);
			case Group17Package.PERIOD__END:
				return END_EDEFAULT == null ? end != null : !END_EDEFAULT.equals(end);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group17Package.PERIOD___OVERLAPS_WITH__PERIOD:
				return overlapsWith((Period)arguments.get(0));
			case Group17Package.PERIOD___IS_DATE_IN_PERIOD__DATE:
				return isDateInPeriod((Date)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (start: ");
		result.append(start);
		result.append(", end: ");
		result.append(end);
		result.append(')');
		return result.toString();
	}

} //PeriodImpl
