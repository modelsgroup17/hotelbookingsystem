/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import java.util.Collection;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;
import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.Period;
import se.chalmers.cse.mdsd1617.group17.Reservation;
import se.chalmers.cse.mdsd1617.group17.ReservationStatus;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getRoom <em>Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getReservationID <em>Reservation ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getExtraCost <em>Extra Cost</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl#getDescriptionOfExtra <em>Description Of Extra</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ReservationImpl extends MinimalEObjectImpl.Container implements Reservation {
	/**
	 * The default value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected static final ReservationStatus STATUS_EDEFAULT = ReservationStatus.CREATED;

	/**
	 * The cached value of the '{@link #getStatus() <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStatus()
	 * @generated
	 * @ordered
	 */
	protected ReservationStatus status = STATUS_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected Period period;

	/**
	 * The cached value of the '{@link #getRoom() <em>Room</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoom()
	 * @generated
	 * @ordered
	 */
	protected Room room;

	/**
	 * The default value of the '{@link #getReservationID() <em>Reservation ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservationID()
	 * @generated
	 * @ordered
	 */
	protected static final int RESERVATION_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getReservationID() <em>Reservation ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservationID()
	 * @generated
	 * @ordered
	 */
	protected int reservationID = RESERVATION_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getRoomType() <em>Room Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRoomType()
	 * @generated
	 * @ordered
	 */
	protected RoomType roomType;

	/**
	 * The default value of the '{@link #getExtraCost() <em>Extra Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraCost()
	 * @generated
	 * @ordered
	 */
	protected static final double EXTRA_COST_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getExtraCost() <em>Extra Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getExtraCost()
	 * @generated
	 * @ordered
	 */
	protected double extraCost = EXTRA_COST_EDEFAULT;

	/**
	 * The cached value of the '{@link #getDescriptionOfExtra() <em>Description Of Extra</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescriptionOfExtra()
	 * @generated
	 * @ordered
	 */
	protected EList<String> descriptionOfExtra;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.RESERVATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ReservationStatus getStatus() {
		return status;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setStatus(ReservationStatus newStatus) {
		this.status = newStatus;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Period getPeriod() {
		if (period != null && period.eIsProxy()) {
			InternalEObject oldPeriod = (InternalEObject)period;
			period = (Period)eResolveProxy(oldPeriod);
			if (period != oldPeriod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group17Package.RESERVATION__PERIOD, oldPeriod, period));
			}
		}
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Period basicGetPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPeriod(Period newPeriod) {
		period = newPeriod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Room getRoom() {
		return room;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoom(Room newRoom, NotificationChain msgs) {
		Room oldRoom = room;
		room = newRoom;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Group17Package.RESERVATION__ROOM, oldRoom, newRoom);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setRoom(Room newRoom) {
		room = newRoom;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getReservationID() {
		return reservationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setReservationID(int newReservationID) {
		reservationID = newReservationID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomType getRoomType() {
		return roomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRoomType(RoomType newRoomType, NotificationChain msgs) {
		RoomType oldRoomType = roomType;
		roomType = newRoomType;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Group17Package.RESERVATION__ROOM_TYPE, oldRoomType, newRoomType);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setRoomType(RoomType newRoomType) {
		this.roomType = newRoomType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getExtraCost() {
		return extraCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setExtraCost(double newExtraCost) {
		extraCost = newExtraCost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getDescriptionOfExtra() {
		if (descriptionOfExtra == null) {
			descriptionOfExtra = new EDataTypeUniqueEList<String>(String.class, this, Group17Package.RESERVATION__DESCRIPTION_OF_EXTRA);
		}
		return descriptionOfExtra;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Group17Package.RESERVATION__ROOM:
				return basicSetRoom(null, msgs);
			case Group17Package.RESERVATION__ROOM_TYPE:
				return basicSetRoomType(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.RESERVATION__STATUS:
				return getStatus();
			case Group17Package.RESERVATION__PERIOD:
				if (resolve) return getPeriod();
				return basicGetPeriod();
			case Group17Package.RESERVATION__ROOM:
				return getRoom();
			case Group17Package.RESERVATION__RESERVATION_ID:
				return getReservationID();
			case Group17Package.RESERVATION__ROOM_TYPE:
				return getRoomType();
			case Group17Package.RESERVATION__EXTRA_COST:
				return getExtraCost();
			case Group17Package.RESERVATION__DESCRIPTION_OF_EXTRA:
				return getDescriptionOfExtra();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.RESERVATION__STATUS:
				setStatus((ReservationStatus)newValue);
				return;
			case Group17Package.RESERVATION__PERIOD:
				setPeriod((Period)newValue);
				return;
			case Group17Package.RESERVATION__ROOM:
				setRoom((Room)newValue);
				return;
			case Group17Package.RESERVATION__RESERVATION_ID:
				setReservationID((Integer)newValue);
				return;
			case Group17Package.RESERVATION__ROOM_TYPE:
				setRoomType((RoomType)newValue);
				return;
			case Group17Package.RESERVATION__EXTRA_COST:
				setExtraCost((Double)newValue);
				return;
			case Group17Package.RESERVATION__DESCRIPTION_OF_EXTRA:
				getDescriptionOfExtra().clear();
				getDescriptionOfExtra().addAll((Collection<? extends String>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.RESERVATION__STATUS:
				setStatus(STATUS_EDEFAULT);
				return;
			case Group17Package.RESERVATION__PERIOD:
				setPeriod((Period)null);
				return;
			case Group17Package.RESERVATION__ROOM:
				setRoom((Room)null);
				return;
			case Group17Package.RESERVATION__RESERVATION_ID:
				setReservationID(RESERVATION_ID_EDEFAULT);
				return;
			case Group17Package.RESERVATION__ROOM_TYPE:
				setRoomType((RoomType)null);
				return;
			case Group17Package.RESERVATION__EXTRA_COST:
				setExtraCost(EXTRA_COST_EDEFAULT);
				return;
			case Group17Package.RESERVATION__DESCRIPTION_OF_EXTRA:
				getDescriptionOfExtra().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.RESERVATION__STATUS:
				return status != STATUS_EDEFAULT;
			case Group17Package.RESERVATION__PERIOD:
				return period != null;
			case Group17Package.RESERVATION__ROOM:
				return room != null;
			case Group17Package.RESERVATION__RESERVATION_ID:
				return reservationID != RESERVATION_ID_EDEFAULT;
			case Group17Package.RESERVATION__ROOM_TYPE:
				return roomType != null;
			case Group17Package.RESERVATION__EXTRA_COST:
				return extraCost != EXTRA_COST_EDEFAULT;
			case Group17Package.RESERVATION__DESCRIPTION_OF_EXTRA:
				return descriptionOfExtra != null && !descriptionOfExtra.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (status: ");
		result.append(status);
		result.append(", reservationID: ");
		result.append(reservationID);
		result.append(", extraCost: ");
		result.append(extraCost);
		result.append(", descriptionOfExtra: ");
		result.append(descriptionOfExtra);
		result.append(')');
		return result.toString();
	}

} //ReservationImpl
