/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import java.lang.reflect.InvocationTargetException;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;

import javax.xml.soap.SOAPException;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires;
import se.chalmers.cse.mdsd1617.group17.Booking;
import se.chalmers.cse.mdsd1617.group17.BookingManager;
import se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.Guest;
import se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides;
import se.chalmers.cse.mdsd1617.group17.Period;
import se.chalmers.cse.mdsd1617.group17.Reservation;
import se.chalmers.cse.mdsd1617.group17.ReservationStatus;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomType;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl#getIroomavailabilitymanagerprovides <em>Iroomavailabilitymanagerprovides</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl#getBookings <em>Bookings</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingManagerImpl extends MinimalEObjectImpl.Container implements BookingManager {
	/**
	 * The cached value of the '{@link #getIroomavailabilitymanagerprovides() <em>Iroomavailabilitymanagerprovides</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getIroomavailabilitymanagerprovides()
	 * @generated
	 * @ordered
	 */
	protected IRoomAccessProvides iroomavailabilitymanagerprovides;
	
	/**
	 * The cached value of the '{@link #getBookings() <em>Bookings</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookings()
	 * @generated
	 * @ordered
	 */
	protected EList<Booking> bookings;

	private static BookingManager instance;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BookingManagerImpl() {
		// We have to allow an empty constructor, as it's used by Jenkins.
		// This does introduce a tight coupling, but we cannot do anything about that.
		this(RoomManagerImpl.getInstance());
	}
	
	public static BookingManager getInstance() {
		if (instance == null) {
			instance = new BookingManagerImpl();
		}
		return instance;
	}

	/**
	 * A reference to the highest booking id
	 */
	private int currentMaxBookingId = 0;
	
	/**
	 * A reference to the highest booking id
	 */
	private int currentMaxReservationId = 0;
	
	private CustomerRequires banking;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BookingManagerImpl(IRoomAccessProvides roomAccessProvider) {
		super();
		try {
			banking = se.chalmers.cse.mdsd1617.banking.customerRequires.CustomerRequires.instance();
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		bookings = new BasicEList<Booking>();
		this.iroomavailabilitymanagerprovides = roomAccessProvider;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.BOOKING_MANAGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomAccessProvides getIroomavailabilitymanagerprovides() {
		if (iroomavailabilitymanagerprovides != null && iroomavailabilitymanagerprovides.eIsProxy()) {
			InternalEObject oldIroomavailabilitymanagerprovides = (InternalEObject)iroomavailabilitymanagerprovides;
			iroomavailabilitymanagerprovides = (IRoomAccessProvides)eResolveProxy(oldIroomavailabilitymanagerprovides);
			if (iroomavailabilitymanagerprovides != oldIroomavailabilitymanagerprovides) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES, oldIroomavailabilitymanagerprovides, iroomavailabilitymanagerprovides));
			}
		}
		return iroomavailabilitymanagerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public IRoomAccessProvides basicGetIroomavailabilitymanagerprovides() {
		return iroomavailabilitymanagerprovides;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIroomavailabilitymanagerprovides(IRoomAccessProvides newIroomavailabilitymanagerprovides) {
		IRoomAccessProvides oldIroomavailabilitymanagerprovides = iroomavailabilitymanagerprovides;
		iroomavailabilitymanagerprovides = newIroomavailabilitymanagerprovides;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES, oldIroomavailabilitymanagerprovides, iroomavailabilitymanagerprovides));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Booking> getBookings() {
		if (bookings == null) {
			bookings = new EObjectContainmentEList<Booking>(Booking.class, this, Group17Package.BOOKING_MANAGER__BOOKINGS);
		}
		return bookings;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public int initiateBooking(String firstName, String startDate, String endDate, String lastName) {
		Booking booking = new BookingImpl();
		Guest guest = new GuestImpl();
		
		if(firstName == null || lastName == null || firstName.isEmpty() || lastName.isEmpty()){
			return -1;
		}
		
		try {
			Period p = new PeriodImpl(startDate, endDate);
			booking.setPeriod(p);
		} catch (ParseException e) {
			return -1;
		}
		
		guest.setFirstName(firstName);
		guest.setLastName(lastName);
		booking.setGuest(guest);
		
		currentMaxBookingId++;
		booking.setBookingID(currentMaxBookingId);
		bookings.add(booking);
		return booking.getBookingID();
	}

	/**
	 * Returns a list of FreeRoomTypesDTO for all available rooms for specified period.
	 * @generated NOT
	 */
	public EList<FreeRoomTypesDTO> getFreeRooms(int numBeds, String startDate, String endDate) {
		EList<Room> rooms = null;
		try {
			rooms = getFreeRoomsList(numBeds, new PeriodImpl(startDate, endDate));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return roomsToFreeRoomTypesDTO(rooms);
	}
	
	/**
	 * Returns a list of all free rooms for a given period
	 * @param nbrOfBeds
	 * @param startDate
	 * @param endDate
	 * @return
	 * @throws Exception 
	 */
	public EList<Room> getFreeRoomsList(int nbrOfBeds, Period period) throws Exception {
		EList<Room> rooms = new BasicEList<Room>();
		for (Room r : iroomavailabilitymanagerprovides.getAllRooms()) {
			if (!iroomavailabilitymanagerprovides.isRoomBlocked(r)) {
				rooms.add(r);
			}
		}
		
		for (Booking booking : bookings) {
			for (Reservation res : booking.getReservations()) {
				if (period.overlapsWith(res.getPeriod())) {
					Room room = res.getRoom();
					if (room != null) {
						rooms.remove(room);
					} else if(res.getRoomType() != null) {
						// Remove a room with room type
						for (Iterator<Room> iterator = rooms.iterator(); iterator.hasNext();) {
						    Room _r = iterator.next();
						    if (res.getRoomType() == _r.getRoomType()) {
						        iterator.remove();
						        break;
						    }
						}

					}
					
					
				}
			}
		}
		return rooms;
	}
	
	/**
	 * Returns a list of free rooms for a specific RoomType.
	 * @param nbrOfBeds
	 * @param startDate
	 * @param endDate
	 * @param roomType
	 * @return
	 * @throws Exception 
	 */
	private EList<Room> getFreeRoomsForType(int nbrOfBeds, Period period, RoomType roomType) throws Exception {
		EList<Room> matches = new BasicEList<Room>();
		for (Room r : getFreeRoomsList(nbrOfBeds, period)) {
			if (r.getRoomType() == roomType) {
				matches.add(r);
			}
		}
		return matches;
	}
	
	private EList<Room> getFreeRoomsForType(String roomTypeDescription, Period period) throws Exception {
		EList<RoomType> types = iroomavailabilitymanagerprovides.getAllRoomTypes();
		for (RoomType type : types) {
			if (type.getName().equals(roomTypeDescription)) {
				return getFreeRoomsForType(type.getNumBeds(), period, type);
			}
		}
		return null;
	}
	
	/**
	 * Takes a list of rooms and converts them into a list of FreeRoomTypesDTO
	 */
	private EList<FreeRoomTypesDTO> roomsToFreeRoomTypesDTO(EList<Room> rooms) {
		EList<FreeRoomTypesDTO> list = new BasicEList<FreeRoomTypesDTO>();
		if (rooms == null) {
			return null;
		}
		
		for (Room room : rooms) {
			if (room == null) {
				break;
			}
			FreeRoomTypesDTO newType = findRoomTypeInList(list, room.getRoomType());
			if (newType == null) {
				newType = new FreeRoomTypesDTOImpl();
				newType.setNumBeds(room.getRoomType().getNumBeds());
				newType.setNumFreeRooms(1);
				newType.setPricePerNight(room.getRoomType().getPrice());
				newType.setRoomTypeDescription(room.getRoomType().getName());
				list.add(newType);
			} else {
				newType.setNumFreeRooms(newType.getNumFreeRooms()+1);
			}
		}
		
		return list;
	}
	
	/**
	 * Finds the FreeRoomTypesDTO in a list with a specific RoomType.
	 * @param list
	 * @param type
	 * @return FreeRoomTypesDTO or null
	 */
	private FreeRoomTypesDTO findRoomTypeInList(EList<FreeRoomTypesDTO> list, RoomType type) {
		for (FreeRoomTypesDTO roomType : list) {
			if (roomType.getRoomTypeDescription().equals(type.getName())) {
				return roomType;
			}
		}
		return null;
	}

	/**
	 * Adds a single room of type roomTypeDescription to a booking
	 * @generated NOT
	 */
	public boolean addRoomToBooking(String roomTypeDescription, int bookingID) {
		Booking booking = getBookingByID(bookingID);
		EList<Room> rooms = null;
		try {
			if (booking == null) {
				return false;
			}
			rooms = getFreeRoomsForType(roomTypeDescription, booking.getPeriod());
			if (rooms == null || rooms.isEmpty()) {
				return false;
			}
			
			// Check that there is no reservations or only reservations with status CREATED
			if (booking.getReservations() != null || !booking.getReservations().isEmpty()) {
				for (Reservation r : booking.getReservations()) {
					if (r.getStatus() != ReservationStatus.CREATED) {
						return false;
					}
				}
			}
			Reservation res = new ReservationImpl();
			currentMaxReservationId++;
			res.setReservationID(currentMaxReservationId);
			res.setPeriod(booking.getPeriod());
			res.setRoomType(rooms.get(0).getRoomType());
			EList<Reservation> reservations = booking.getReservations();
			reservations.add(res);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Sets the specified booking to confirmed
	 * @generated NOT
	 */
	public boolean confirmBooking(int bookingID) {
		if (bookingID < 0) {
			return false;
		}
		
		Booking booking = getBookingByID(bookingID);
		
		if (booking != null && !booking.getReservations().isEmpty()) {
			EList<Reservation> list = booking.getReservations();
			if (list == null) {
				return false;
			} else {
				for (Reservation r: booking.getReservations()) {
					r.setStatus(ReservationStatus.CONFIRMED);
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public boolean payDuringCheckout(String ccNumber, String ccv, int expiryMonth, int expiryYear, String firstName, String lastName) {
		double sum = 0.0;
		
		//check if the credit card is valid
		try {
			if(banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)){				
				for(Booking booking : bookings){
					if(booking.getGuest().getFirstName().equals(firstName) && booking.getGuest().getLastName().equals(lastName)) {
						sum = booking.getTotalPriceLeftToPay();
					}
					//check out the booking if the customer can pay
					if(banking.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum)) {
						for (Reservation r: booking.getReservations()) {
							if (r.getStatus() == ReservationStatus.INITIATECHECKOUT) {
								r.setStatus(ReservationStatus.CHECKEDOUT);								
							}
						}
						
						return true;
					}
					
					return false;
				}	
			}
		} catch (SOAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * Checks in a single room
	 * 
	 * @generated NOT
	 */
	public int checkInRoom(String roomTypeDescription, int bookingID) {
		Booking booking = getBookingByID(bookingID);
		if (booking == null) {
			return -1;
		}
		for(Reservation r: booking.getReservations()) {
			if (r.getStatus() == ReservationStatus.CONFIRMED && r.getRoomType() != null) {
				//adds a physical room to the reservation
				try {
					RoomType roomType = r.getRoomType();
					r.setRoomType(null);
					EList<Room> rooms = getFreeRoomsForType(roomType.getName(), r.getPeriod());
					if(rooms != null && !rooms.isEmpty()) {
						r.setRoom(rooms.get(0));
						r.setStatus(ReservationStatus.CHECKEDIN);
						return r.getRoom().getRoomNbr();
					} else {
						throw new IllegalArgumentException();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		return -1;
	}

	/**
	 * The user provides booking ID, room number and his/her credit card
	 * details. Makes the payment.
	 * 
	 * @generated NOT
	 */
	public boolean payRoomDuringCheckout(int roomNumber, String ccNumber, String ccv, int expiryMonth, int expiryYear,
			String firstName, String lastName) {

		Booking booking = null;
		Reservation reservation = null;

		for (Booking b : bookings) {
			Guest guest = b.getGuest();			
			if (guest.getFirstName().equals(firstName) && guest.getLastName().equals(lastName)) {
				for (Reservation r : b.getReservations()) {
					if (r.getStatus() == ReservationStatus.INITIATECHECKOUT && r.getRoom().getRoomNbr() == roomNumber) {
						booking = b;
						reservation = r;
						break;
					}
				}
			}
		}

		if (reservation == null || booking == null) {
			return false;
		}

		double sum = reservation.getRoom().getRoomType().getPrice() + reservation.getExtraCost();

		try {
			if (banking.isCreditCardValid(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName)) {
				if (banking.makePayment(ccNumber, ccv, expiryMonth, expiryYear, firstName, lastName, sum)) {
					reservation.setStatus(ReservationStatus.CHECKEDOUT);
					return true;
				}
			} else {
				return false;
			}	
		} catch (SOAPException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	private double _initiateCheckout(int bookingID, Integer roomNumber) {
		Booking booking = getBookingByID(bookingID);
		if (booking == null) {
			return -24;
		}
		
		double totalPrice = 0;
			
		//change the status for the reservations that are checked in
		for(Reservation r: booking.getReservations()) {
			boolean isSelectedRoom = roomNumber == null || (r.getRoom() != null && roomNumber == r.getRoom().getRoomNbr());
			if(r.getStatus() == ReservationStatus.CHECKEDIN && isSelectedRoom) {
				r.setStatus(ReservationStatus.INITIATECHECKOUT);
				Room room = r.getRoom();
				totalPrice = totalPrice + room.getRoomType().getPrice() + r.getExtraCost();
			}
			booking.setTotalPriceLeftToPay(totalPrice);
		}
		return totalPrice;
	}

	/**
	 * <!-- begin-user-doc -->
	 * calculates what the guest should pay
	 * checks if the reservations are checked in  
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public double initiateCheckout(int bookingID) {
		// When checking out whole booking, we have to lock down for more checkouts until
		// payment is done.
		for (Booking booking : bookings) {
			for (Reservation reservation : booking.getReservations()) {
				if (reservation.getStatus() == ReservationStatus.INITIATECHECKOUT) {
					// Checkout already started -- abort
					return -42;
				}
			}
		}
		
		return _initiateCheckout(bookingID, null);
	}

	/**
	 * Checks out a single room
	 * 
	 * @generated NOT
	 */
	public double initiateRoomCheckout(int roomNumber, int bookingID) {
		return _initiateCheckout(bookingID, roomNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * Changes the total number of rooms a booking has. 
	 * Edits startDate and endDate for all reservations in a booking. 
	 * Changes the roomTypes for all rooms belonging to a booking.  
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void editABooking(int bookingID, RoomType roomType, Date startDate, Date endDate, int nbrOfRooms) {
		Booking booking = getBookingByID(bookingID);
		Period per;
		try {
			per = new PeriodImpl(startDate, endDate);

			for(Reservation r: booking.getReservations()) {
				//Make sure that you can not edit a booking if its been checked in. 
				if(r.getStatus() == ReservationStatus.CHECKEDIN || r.getStatus() == ReservationStatus.CHECKEDOUT || r.getStatus() == ReservationStatus.INITIATECHECKOUT) {
					throw new IllegalArgumentException("you can not edit a booking that has been checked in");
				}
			}

			//check if we have the number of rooms for that type
			EList<Room> listOfRooms = getFreeRoomsForType(roomType.getName(), per);
			if (listOfRooms.size() < nbrOfRooms) {
				return;
			}
			//remove all the reservations in the booking
			booking.getReservations().clear();
			booking.setPeriod(per);
			
			//add reservations
			for (int i = 0; i<nbrOfRooms; i++) {
				this.addRoomToBooking(roomType.getName(), bookingID);
			}

		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * returns a Booking with a specific bookingID
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public Booking getBookingByID(int bookingID) {
		if (bookings == null || bookings.isEmpty()) {
			return null;
		}
		
		for(Booking b: bookings) {
			if (b != null && b.getBookingID() == bookingID) {
				return b;
			}
		}

		return null;
	}

	/**
	 * Cancels a booking
	 * @generated NOT
	 */
	public void cancelABooking(int bookingID) {
		Booking booking = getBookingByID(bookingID);
		bookings.remove(booking);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listBookings() {
		EList<Booking> list = new BasicEList<Booking>();
		for(Booking booking : bookings){
			for(Reservation reservation : booking.getReservations()){
				if(reservation.getStatus() == ReservationStatus.CONFIRMED){
					list.add(booking);
					break;
				}
			}
			
		}
		return list;
	}

	/**
	 * Checks if the rooms in a specified date is booked
	 * 
	 * @generated NOT
	 */
	public EList<Booking> occupiedRoomsForDate(Date date) {
		
		EList<Booking> occupiedList = new BasicEList<Booking>();
		for(Booking booking : bookings){
			for (Reservation reservation :booking.getReservations()){
				if (reservation.getPeriod().isDateInPeriod(date)){
					occupiedList.add(booking);
				}
			}
		}
		return occupiedList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckInsForDate(Date date) {		
		EList<Booking> checkInList = new BasicEList<Booking>();
		if(date.after(new Date())){
			throw new IllegalArgumentException("Cannot search in the future");
		}

		for(Booking booking: this.getBookings()){

			for(Reservation reservation : booking.getReservations()){

				if(reservation.getPeriod().getStart().equals(date)){
					checkInList.add(booking);
					break;
				}
			}
		}

		return checkInList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckOutsForDate(Date date) {
		EList<Booking> checkInList = new BasicEList<Booking>();
		if(date.after(new Date())){
			throw new IllegalArgumentException();
		}

		for(Booking booking: this.getBookings()){

			for(Reservation reservation : booking.getReservations()){

				if(reservation.getPeriod().getEnd().equals(date)){
					checkInList.add(booking);
					break;
				}
			}
		}

		return checkInList;	
	}


	/**
	 * <!-- begin-user-doc -->
	 * Adds extra cost to the total extra cost for a specific room,
	 * also adds a description of the extra cost in a list
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void addExtraCostsForRoom(int bookingID, int reservationID, String descriptionOfExtra, double priceOfExtra) {
		Booking booking = getBookingByID(bookingID);
		for(Reservation r: booking.getReservations()) {
			if(r.getReservationID() == (reservationID)) {
				r.setExtraCost(r.getExtraCost() + priceOfExtra);
				r.getDescriptionOfExtra().add(descriptionOfExtra);
			}	
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * The user provides the booking ID. 
	 * The system shows the available (physical) rooms to choose from according to the booking information. 
	 * The actor chooses the rooms and the system marks them as occupied and links them to the booking.
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void checkInBooking(int bookingID) {
		Booking booking = getBookingByID(bookingID);
		if (booking == null) {
			return;
		}
		for(Reservation r: booking.getReservations()) {
			if(r.getStatus() == ReservationStatus.CONFIRMED) {
				//adds a physical room to the reservation
				try {
					RoomType roomType = r.getRoomType();
					r.setRoomType(null);
					EList<Room> rooms = getFreeRoomsForType(roomType.getName(), r.getPeriod());
					if(rooms != null && !rooms.isEmpty()) {
						r.setRoom(rooms.get(0));
					} else {
						throw new IllegalArgumentException();
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(r.getStatus() == ReservationStatus.CONFIRMED);
				r.setStatus(ReservationStatus.CHECKEDIN);
			}
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckInsForDate(Date startDate, Date endDate) {
		EList<Booking> checkInList = new BasicEList<Booking>();		
		Date date = new Date();
		Calendar cal = new GregorianCalendar();
		Date now = new Date();
		
		if(startDate.after(endDate) || startDate.after(now) || endDate.after(now)){
			throw new IllegalArgumentException();
		}
		date = startDate;
		while (!date.after(endDate)){
			
			checkInList.addAll(this.listCheckInsForDate(date));
			
	        cal.setTime(date);
	        cal.add(Calendar.DATE, 1); 
	        date = cal.getTime();
		}
		
		return checkInList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public EList<Booking> listCheckOutsForDate(Date startDate, Date endDate) {
		EList<Booking> checkOutList = new BasicEList<Booking>();		
		Date date = new Date();
		Calendar cal = new GregorianCalendar();
		date = cal.getTime();
		
		if(startDate.after(endDate) || startDate.after(date) || endDate.after(date)){
			throw new IllegalArgumentException();
		}
		date = startDate;
		while (!date.after(endDate)){
			
			checkOutList.addAll(this.listCheckOutsForDate(date));
			
	        cal.setTime(date);
	        cal.add(Calendar.DATE, 1); 
	        date = cal.getTime();			
		}		
		return checkOutList;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void clearBookings() {
		bookings.clear();
		currentMaxBookingId = 0;
		currentMaxReservationId = 0;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Group17Package.BOOKING_MANAGER__BOOKINGS:
				return ((InternalEList<?>)getBookings()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES:
				if (resolve) return getIroomavailabilitymanagerprovides();
				return basicGetIroomavailabilitymanagerprovides();
			case Group17Package.BOOKING_MANAGER__BOOKINGS:
				return getBookings();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES:
				setIroomavailabilitymanagerprovides((IRoomAccessProvides)newValue);
				return;
			case Group17Package.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				getBookings().addAll((Collection<? extends Booking>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES:
				setIroomavailabilitymanagerprovides((IRoomAccessProvides)null);
				return;
			case Group17Package.BOOKING_MANAGER__BOOKINGS:
				getBookings().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.BOOKING_MANAGER__IROOMAVAILABILITYMANAGERPROVIDES:
				return iroomavailabilitymanagerprovides != null;
			case Group17Package.BOOKING_MANAGER__BOOKINGS:
				return bookings != null && !bookings.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case Group17Package.BOOKING_MANAGER___INITIATE_BOOKING__STRING_STRING_STRING_STRING:
				return initiateBooking((String)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (String)arguments.get(3));
			case Group17Package.BOOKING_MANAGER___GET_FREE_ROOMS__INT_STRING_STRING:
				return getFreeRooms((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2));
			case Group17Package.BOOKING_MANAGER___ADD_ROOM_TO_BOOKING__STRING_INT:
				return addRoomToBooking((String)arguments.get(0), (Integer)arguments.get(1));
			case Group17Package.BOOKING_MANAGER___CONFIRM_BOOKING__INT:
				return confirmBooking((Integer)arguments.get(0));
			case Group17Package.BOOKING_MANAGER___PAY_DURING_CHECKOUT__STRING_STRING_INT_INT_STRING_STRING:
				return payDuringCheckout((String)arguments.get(0), (String)arguments.get(1), (Integer)arguments.get(2), (Integer)arguments.get(3), (String)arguments.get(4), (String)arguments.get(5));
			case Group17Package.BOOKING_MANAGER___CHECK_IN_ROOM__STRING_INT:
				return checkInRoom((String)arguments.get(0), (Integer)arguments.get(1));
			case Group17Package.BOOKING_MANAGER___PAY_ROOM_DURING_CHECKOUT__INT_STRING_STRING_INT_INT_STRING_STRING:
				return payRoomDuringCheckout((Integer)arguments.get(0), (String)arguments.get(1), (String)arguments.get(2), (Integer)arguments.get(3), (Integer)arguments.get(4), (String)arguments.get(5), (String)arguments.get(6));
			case Group17Package.BOOKING_MANAGER___INITIATE_CHECKOUT__INT:
				return initiateCheckout((Integer)arguments.get(0));
			case Group17Package.BOOKING_MANAGER___INITIATE_ROOM_CHECKOUT__INT_INT:
				return initiateRoomCheckout((Integer)arguments.get(0), (Integer)arguments.get(1));
			case Group17Package.BOOKING_MANAGER___EDIT_ABOOKING__INT_ROOMTYPE_DATE_DATE_INT:
				editABooking((Integer)arguments.get(0), (RoomType)arguments.get(1), (Date)arguments.get(2), (Date)arguments.get(3), (Integer)arguments.get(4));
				return null;
			case Group17Package.BOOKING_MANAGER___CANCEL_ABOOKING__INT:
				cancelABooking((Integer)arguments.get(0));
				return null;
			case Group17Package.BOOKING_MANAGER___LIST_BOOKINGS:
				return listBookings();
			case Group17Package.BOOKING_MANAGER___OCCUPIED_ROOMS_FOR_DATE__DATE:
				return occupiedRoomsForDate((Date)arguments.get(0));
			case Group17Package.BOOKING_MANAGER___LIST_CHECK_INS_FOR_DATE__DATE:
				return listCheckInsForDate((Date)arguments.get(0));
			case Group17Package.BOOKING_MANAGER___LIST_CHECK_OUTS_FOR_DATE__DATE:
				return listCheckOutsForDate((Date)arguments.get(0));
			case Group17Package.BOOKING_MANAGER___ADD_EXTRA_COSTS_FOR_ROOM__INT_INT_STRING_DOUBLE:
				addExtraCostsForRoom((Integer)arguments.get(0), (Integer)arguments.get(1), (String)arguments.get(2), (Double)arguments.get(3));
				return null;
			case Group17Package.BOOKING_MANAGER___CHECK_IN_BOOKING__INT:
				checkInBooking((Integer)arguments.get(0));
				return null;
			case Group17Package.BOOKING_MANAGER___LIST_CHECK_INS_FOR_DATE__DATE_DATE:
				return listCheckInsForDate((Date)arguments.get(0), (Date)arguments.get(1));
			case Group17Package.BOOKING_MANAGER___LIST_CHECK_OUTS_FOR_DATE__DATE_DATE:
				return listCheckOutsForDate((Date)arguments.get(0), (Date)arguments.get(1));
			case Group17Package.BOOKING_MANAGER___CLEAR_BOOKINGS:
				clearBookings();
				return null;
		}
		return super.eInvoke(operationID, arguments);
	}

} //BookingManagerImpl
