/**
 */
package se.chalmers.cse.mdsd1617.group17.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EDataTypeUniqueEList;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import se.chalmers.cse.mdsd1617.group17.Booking;
import se.chalmers.cse.mdsd1617.group17.Group17Package;
import se.chalmers.cse.mdsd1617.group17.Guest;
import se.chalmers.cse.mdsd1617.group17.Period;
import se.chalmers.cse.mdsd1617.group17.Reservation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getSpecialRequest <em>Special Request</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getGuest <em>Guest</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getPeriod <em>Period</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.impl.BookingImpl#getTotalPriceLeftToPay <em>Total Price Left To Pay</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BookingImpl extends MinimalEObjectImpl.Container implements Booking {
	/**
	 * The cached value of the '{@link #getSpecialRequest() <em>Special Request</em>}' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSpecialRequest()
	 * @generated
	 * @ordered
	 */
	protected EList<String> specialRequest;

	/**
	 * The cached value of the '{@link #getReservations() <em>Reservations</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReservations()
	 * @generated
	 * @ordered
	 */
	protected EList<Reservation> reservations;

	/**
	 * The cached value of the '{@link #getGuest() <em>Guest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getGuest()
	 * @generated
	 * @ordered
	 */
	protected Guest guest;

	/**
	 * The default value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected static final int BOOKING_ID_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getBookingID() <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getBookingID()
	 * @generated
	 * @ordered
	 */
	protected int bookingID = BOOKING_ID_EDEFAULT;

	/**
	 * The cached value of the '{@link #getPeriod() <em>Period</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPeriod()
	 * @generated
	 * @ordered
	 */
	protected Period period;

	/**
	 * The default value of the '{@link #getTotalPriceLeftToPay() <em>Total Price Left To Pay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPriceLeftToPay()
	 * @generated
	 * @ordered
	 */
	protected static final double TOTAL_PRICE_LEFT_TO_PAY_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getTotalPriceLeftToPay() <em>Total Price Left To Pay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTotalPriceLeftToPay()
	 * @generated
	 * @ordered
	 */
	protected double totalPriceLeftToPay = TOTAL_PRICE_LEFT_TO_PAY_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public BookingImpl() {
		super();
		reservations = new BasicEList<Reservation>();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Group17Package.Literals.BOOKING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<String> getSpecialRequest() {
		if (specialRequest == null) {
			specialRequest = new EDataTypeUniqueEList<String>(String.class, this, Group17Package.BOOKING__SPECIAL_REQUEST);
		}
		return specialRequest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Reservation> getReservations() {
		if (reservations == null) {
			reservations = new EObjectContainmentEList<Reservation>(Reservation.class, this, Group17Package.BOOKING__RESERVATIONS);
		}
		return reservations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Guest getGuest() {
		return guest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetGuest(Guest newGuest, NotificationChain msgs) {
		Guest oldGuest = guest;
		guest = newGuest;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Group17Package.BOOKING__GUEST, oldGuest, newGuest);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setGuest(Guest newGuest) {
		guest = newGuest;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getBookingID() {
		return bookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setBookingID(int newBookingID) {
		bookingID = newBookingID;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Period getPeriod() {
		if (period != null && period.eIsProxy()) {
			InternalEObject oldPeriod = (InternalEObject)period;
			period = (Period)eResolveProxy(oldPeriod);
			if (period != oldPeriod) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Group17Package.BOOKING__PERIOD, oldPeriod, period));
			}
		}
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Period basicGetPeriod() {
		return period;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setPeriod(Period newPeriod) {
		period = newPeriod;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalPriceLeftToPay() {
		return totalPriceLeftToPay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	public void setTotalPriceLeftToPay(double newTotalPriceLeftToPay) {
		totalPriceLeftToPay = newTotalPriceLeftToPay;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Group17Package.BOOKING__RESERVATIONS:
				return ((InternalEList<?>)getReservations()).basicRemove(otherEnd, msgs);
			case Group17Package.BOOKING__GUEST:
				return basicSetGuest(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Group17Package.BOOKING__SPECIAL_REQUEST:
				return getSpecialRequest();
			case Group17Package.BOOKING__RESERVATIONS:
				return getReservations();
			case Group17Package.BOOKING__GUEST:
				return getGuest();
			case Group17Package.BOOKING__BOOKING_ID:
				return getBookingID();
			case Group17Package.BOOKING__PERIOD:
				if (resolve) return getPeriod();
				return basicGetPeriod();
			case Group17Package.BOOKING__TOTAL_PRICE_LEFT_TO_PAY:
				return getTotalPriceLeftToPay();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Group17Package.BOOKING__SPECIAL_REQUEST:
				getSpecialRequest().clear();
				getSpecialRequest().addAll((Collection<? extends String>)newValue);
				return;
			case Group17Package.BOOKING__RESERVATIONS:
				getReservations().clear();
				getReservations().addAll((Collection<? extends Reservation>)newValue);
				return;
			case Group17Package.BOOKING__GUEST:
				setGuest((Guest)newValue);
				return;
			case Group17Package.BOOKING__BOOKING_ID:
				setBookingID((Integer)newValue);
				return;
			case Group17Package.BOOKING__PERIOD:
				setPeriod((Period)newValue);
				return;
			case Group17Package.BOOKING__TOTAL_PRICE_LEFT_TO_PAY:
				setTotalPriceLeftToPay((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Group17Package.BOOKING__SPECIAL_REQUEST:
				getSpecialRequest().clear();
				return;
			case Group17Package.BOOKING__RESERVATIONS:
				getReservations().clear();
				return;
			case Group17Package.BOOKING__GUEST:
				setGuest((Guest)null);
				return;
			case Group17Package.BOOKING__BOOKING_ID:
				setBookingID(BOOKING_ID_EDEFAULT);
				return;
			case Group17Package.BOOKING__PERIOD:
				setPeriod((Period)null);
				return;
			case Group17Package.BOOKING__TOTAL_PRICE_LEFT_TO_PAY:
				setTotalPriceLeftToPay(TOTAL_PRICE_LEFT_TO_PAY_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Group17Package.BOOKING__SPECIAL_REQUEST:
				return specialRequest != null && !specialRequest.isEmpty();
			case Group17Package.BOOKING__RESERVATIONS:
				return reservations != null && !reservations.isEmpty();
			case Group17Package.BOOKING__GUEST:
				return guest != null;
			case Group17Package.BOOKING__BOOKING_ID:
				return bookingID != BOOKING_ID_EDEFAULT;
			case Group17Package.BOOKING__PERIOD:
				return period != null;
			case Group17Package.BOOKING__TOTAL_PRICE_LEFT_TO_PAY:
				return totalPriceLeftToPay != TOTAL_PRICE_LEFT_TO_PAY_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (specialRequest: ");
		result.append(specialRequest);
		result.append(", bookingID: ");
		result.append(bookingID);
		result.append(", totalPriceLeftToPay: ");
		result.append(totalPriceLeftToPay);
		result.append(')');
		return result.toString();
	}

} //BookingImpl
