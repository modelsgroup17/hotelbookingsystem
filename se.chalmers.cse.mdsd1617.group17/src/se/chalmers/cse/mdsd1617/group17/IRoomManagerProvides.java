/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>IRoom Manager Provides</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getIRoomManagerProvides()
 * @model interface="true" abstract="true"
 * @generated
 */
public interface IRoomManagerProvides extends EObject {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomTypeNameRequired="true" roomTypeNameOrdered="false" priceRequired="true" priceOrdered="false" numBedsRequired="true" numBedsOrdered="false" additionalFeaturesRequired="true" additionalFeaturesOrdered="false"
	 * @generated
	 */
	void addRoomType(String roomTypeName, double price, int numBeds, String additionalFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model oldRoomTypeNameRequired="true" oldRoomTypeNameOrdered="false" newRoomTypeNameRequired="true" newRoomTypeNameOrdered="false" priceRequired="true" priceOrdered="false" numBedsRequired="true" numBedsOrdered="false" additionalFeaturesRequired="true" additionalFeaturesOrdered="false"
	 * @generated
	 */
	void updateRoomType(String oldRoomTypeName, String newRoomTypeName, double price, int numBeds, String additionalFeatures);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model romeTypeRequired="true" romeTypeOrdered="false"
	 * @generated
	 */
	void removeRoomType(RoomType romeType);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNbrRequired="true" roomNbrOrdered="false" roomTypeNameRequired="true" roomTypeNameOrdered="false"
	 * @generated
	 */
	void addRoom(int roomNbr, String roomTypeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNbrRequired="true" roomNbrOrdered="false" roomTypeNameRequired="true" roomTypeNameOrdered="false"
	 * @generated
	 */
	void changeRoomTypeOfRoom(int roomNbr, String roomTypeName);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	void removeRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	void blockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	void unblockRoom(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ordered="false" roomNbrRequired="true" roomNbrOrdered="false"
	 * @generated
	 */
	boolean isRoomBlocked(int roomNbr);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void clearRooms();

} // IRoomManagerProvides
