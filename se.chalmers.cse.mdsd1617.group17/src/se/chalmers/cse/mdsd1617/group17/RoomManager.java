/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.common.util.EList;
import se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getRoomTypes <em>Room Types</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getBlockedRooms <em>Blocked Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.RoomManager#getRooms <em>Rooms</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoomManager()
 * @model
 * @generated
 */
public interface RoomManager extends IRoomManagerProvides, IRoomAccessProvides {
	/**
	 * Returns the value of the '<em><b>Room Types</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group17.RoomType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Types</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoomManager_RoomTypes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RoomType> getRoomTypes();

	/**
	 * Returns the value of the '<em><b>Blocked Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group17.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Blocked Rooms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Blocked Rooms</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoomManager_BlockedRooms()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Room> getBlockedRooms();

	/**
	 * Returns the value of the '<em><b>Rooms</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group17.Room}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rooms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rooms</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getRoomManager_Rooms()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Room> getRooms();

	AdministratorRequires getBankingAdmin();

} // RoomManager
