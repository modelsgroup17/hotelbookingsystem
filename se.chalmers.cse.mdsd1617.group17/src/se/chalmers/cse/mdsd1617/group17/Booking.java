/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Booking</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getSpecialRequest <em>Special Request</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getReservations <em>Reservations</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getGuest <em>Guest</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getBookingID <em>Booking ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getPeriod <em>Period</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Booking#getTotalPriceLeftToPay <em>Total Price Left To Pay</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking()
 * @model
 * @generated
 */
public interface Booking extends EObject {
	/**
	 * Returns the value of the '<em><b>Special Request</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Special Request</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Special Request</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_SpecialRequest()
	 * @model ordered="false"
	 * @generated
	 */
	EList<String> getSpecialRequest();

	/**
	 * Returns the value of the '<em><b>Reservations</b></em>' containment reference list.
	 * The list contents are of type {@link se.chalmers.cse.mdsd1617.group17.Reservation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservations</em>' containment reference list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_Reservations()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Reservation> getReservations();

	/**
	 * Returns the value of the '<em><b>Guest</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Guest</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Guest</em>' containment reference.
	 * @see #setGuest(Guest)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_Guest()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Guest getGuest();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Booking#getGuest <em>Guest</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Guest</em>' containment reference.
	 * @see #getGuest()
	 * @generated
	 */
	void setGuest(Guest value);

	/**
	 * Returns the value of the '<em><b>Booking ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Booking ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Booking ID</em>' attribute.
	 * @see #setBookingID(int)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_BookingID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getBookingID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Booking#getBookingID <em>Booking ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Booking ID</em>' attribute.
	 * @see #getBookingID()
	 * @generated
	 */
	void setBookingID(int value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' reference.
	 * @see #setPeriod(Period)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_Period()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Period getPeriod();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Booking#getPeriod <em>Period</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' reference.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(Period value);

	/**
	 * Returns the value of the '<em><b>Total Price Left To Pay</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Price Left To Pay</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Price Left To Pay</em>' attribute.
	 * @see #setTotalPriceLeftToPay(double)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getBooking_TotalPriceLeftToPay()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getTotalPriceLeftToPay();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Booking#getTotalPriceLeftToPay <em>Total Price Left To Pay</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Price Left To Pay</em>' attribute.
	 * @see #getTotalPriceLeftToPay()
	 * @generated
	 */
	void setTotalPriceLeftToPay(double value);

} // Booking
