/**
 */
package se.chalmers.cse.mdsd1617.group17;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Reservation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getStatus <em>Status</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getPeriod <em>Period</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoom <em>Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getReservationID <em>Reservation ID</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoomType <em>Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getExtraCost <em>Extra Cost</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Reservation#getDescriptionOfExtra <em>Description Of Extra</em>}</li>
 * </ul>
 *
 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation()
 * @model
 * @generated
 */
public interface Reservation extends EObject {
	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link se.chalmers.cse.mdsd1617.group17.ReservationStatus}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group17.ReservationStatus
	 * @see #setStatus(ReservationStatus)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_Status()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	ReservationStatus getStatus();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see se.chalmers.cse.mdsd1617.group17.ReservationStatus
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(ReservationStatus value);

	/**
	 * Returns the value of the '<em><b>Period</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Period</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Period</em>' reference.
	 * @see #setPeriod(Period)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_Period()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	Period getPeriod();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getPeriod <em>Period</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Period</em>' reference.
	 * @see #getPeriod()
	 * @generated
	 */
	void setPeriod(Period value);

	/**
	 * Returns the value of the '<em><b>Room</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room</em>' containment reference.
	 * @see #setRoom(Room)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_Room()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	Room getRoom();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoom <em>Room</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room</em>' containment reference.
	 * @see #getRoom()
	 * @generated
	 */
	void setRoom(Room value);

	/**
	 * Returns the value of the '<em><b>Reservation ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Reservation ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Reservation ID</em>' attribute.
	 * @see #setReservationID(int)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_ReservationID()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	int getReservationID();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getReservationID <em>Reservation ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Reservation ID</em>' attribute.
	 * @see #getReservationID()
	 * @generated
	 */
	void setReservationID(int value);

	/**
	 * Returns the value of the '<em><b>Room Type</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Room Type</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Room Type</em>' containment reference.
	 * @see #setRoomType(RoomType)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_RoomType()
	 * @model containment="true" required="true" ordered="false"
	 * @generated
	 */
	RoomType getRoomType();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getRoomType <em>Room Type</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Room Type</em>' containment reference.
	 * @see #getRoomType()
	 * @generated
	 */
	void setRoomType(RoomType value);

	/**
	 * Returns the value of the '<em><b>Extra Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Extra Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Extra Cost</em>' attribute.
	 * @see #setExtraCost(double)
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_ExtraCost()
	 * @model required="true" ordered="false"
	 * @generated
	 */
	double getExtraCost();

	/**
	 * Sets the value of the '{@link se.chalmers.cse.mdsd1617.group17.Reservation#getExtraCost <em>Extra Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Extra Cost</em>' attribute.
	 * @see #getExtraCost()
	 * @generated
	 */
	void setExtraCost(double value);

	/**
	 * Returns the value of the '<em><b>Description Of Extra</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Description Of Extra</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Description Of Extra</em>' attribute list.
	 * @see se.chalmers.cse.mdsd1617.group17.Group17Package#getReservation_DescriptionOfExtra()
	 * @model ordered="false"
	 * @generated
	 */
	EList<String> getDescriptionOfExtra();

} // Reservation
