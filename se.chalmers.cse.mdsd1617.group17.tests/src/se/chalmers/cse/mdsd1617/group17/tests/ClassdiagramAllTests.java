/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Classdiagram</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class ClassdiagramAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new ClassdiagramAllTests("Classdiagram Tests");
		suite.addTest(Group17Tests.suite());
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClassdiagramAllTests(String name) {
		super(name);
	}

} //ClassdiagramAllTests
