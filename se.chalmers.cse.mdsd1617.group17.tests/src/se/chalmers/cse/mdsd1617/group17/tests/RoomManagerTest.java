/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import se.chalmers.cse.mdsd1617.group17.Group17Factory;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomManager;
import se.chalmers.cse.mdsd1617.group17.RoomType;
import se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Room Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoomType(se.chalmers.cse.mdsd1617.group17.RoomType) <em>Remove Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoom(int, java.lang.String) <em>Add Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#changeRoomTypeOfRoom(int, java.lang.String) <em>Change Room Type Of Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoom(int) <em>Remove Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#blockRoom(int) <em>Block Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#unblockRoom(int) <em>Unblock Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#isRoomBlocked(int) <em>Is Room Blocked</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#clearRooms() <em>Clear Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#reserveRoomType(se.chalmers.cse.mdsd1617.group17.RoomType, se.chalmers.cse.mdsd1617.group17.Period) <em>Reserve Room Type</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#assignRoom(se.chalmers.cse.mdsd1617.group17.RoomType, se.chalmers.cse.mdsd1617.group17.Period) <em>Assign Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRooms() <em>Get All Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#isRoomBlocked(se.chalmers.cse.mdsd1617.group17.Room) <em>Is Room Blocked</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRoomTypes() <em>Get All Room Types</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class RoomManagerTest extends TestCase {

	/**
	 * The fixture for this Room Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomManager fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(RoomManagerTest.class);
	}

	/**
	 * Constructs a new Room Manager test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RoomManagerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Room Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void setFixture(RoomManager fixture) {
		this.fixture = fixture;
		if (fixture != null) {
			fixture.clearRooms();
		}
	}

	/**
	 * Returns the fixture for this Room Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RoomManager getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Group17Factory.eINSTANCE.createRoomManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoomType(java.lang.String, double, int, java.lang.String) <em>Add Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoomType(java.lang.String, double, int, java.lang.String)
	 * @generated NOT
	 * Tests UC 2.2.1
	 */
	public void testAddRoomType__String_double_int_String() {
		assertEquals(0, fixture.getRoomTypes().size());
		
		fixture.addRoomType("foo", 10.0, 11, "bar");
		assertEquals(1, fixture.getRoomTypes().size());
		
		RoomType t = fixture.getRoomTypes().get(0);
		assertEquals("foo", t.getName());
		assertEquals(10.0, t.getPrice());
		assertEquals(11, t.getNumBeds());
		assertEquals("bar", t.getAdditionalFeatures());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String) <em>Update Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#updateRoomType(java.lang.String, java.lang.String, double, int, java.lang.String)
	 * @generated NOT
	 * Tests UC 2.2.2
	 */
	public void testUpdateRoomType__String_String_double_int_String() {
		fixture.addRoomType("foo", 10, 11, "bar");
		RoomType t = fixture.getRoomTypes().get(0);
		assertEquals("foo", t.getName());
		
		fixture.updateRoomType("foo", "bar", 1.0, 2, "baz");
		assertEquals("bar", t.getName());
		assertEquals(1.0, t.getPrice());
		assertEquals(2, t.getNumBeds());
		assertEquals("baz", t.getAdditionalFeatures());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoomType(se.chalmers.cse.mdsd1617.group17.RoomType) <em>Remove Room Type</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoomType(se.chalmers.cse.mdsd1617.group17.RoomType)
	 * @generated NOT
	 * Tests UC 2.2.3
	 */
	public void testRemoveRoomType__RoomType() {
		fixture.addRoomType("foo", 10, 11, "bar");
		fixture.addRoomType("fooa", 12, 13, "bary");
		
		assertEquals(2, fixture.getRoomTypes().size());
		RoomType t1 = fixture.getRoomTypes().get(0);
		RoomType t2 = fixture.getRoomTypes().get(1);
		
		fixture.removeRoomType(t1);
		assertEquals(1, fixture.getRoomTypes().size());
		fixture.removeRoomType(t2);
		assertEquals(0, fixture.getRoomTypes().size());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoom(int, java.lang.String) <em>Add Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#addRoom(int, java.lang.String)
	 * @generated NOT
	 * Tests UC 2.2.4
	 */
	public void testAddRoom__int_String() {
		assertEquals(0, fixture.getRooms().size());
		fixture.addRoomType("foo", 10, 11, "bar");
		RoomType roomType = fixture.getRoomTypes().get(0);
		
		fixture.addRoom(100, "foo");
		assertEquals(1, fixture.getRooms().size());
		
		Room room = fixture.getRooms().get(0);
		assertEquals(100, room.getRoomNbr());
		assertEquals(roomType, room.getRoomType());
	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#changeRoomTypeOfRoom(int, java.lang.String) <em>Change Room Type Of Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#changeRoomTypeOfRoom(int, java.lang.String)
	 * @generated NOT
	 * Tests UC 2.2.5
	 */
	public void testChangeRoomTypeOfRoom__int_String() {
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoomType("bar", 12, 13, "baz");

		fixture.addRoom(100, "foo");
		Room room = fixture.getRooms().get(0);
		assertEquals("foo", room.getRoomType().getName());
		
		fixture.changeRoomTypeOfRoom(100, "bar");
		assertEquals("bar", room.getRoomType().getName());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoom(int) <em>Remove Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#removeRoom(int)
	 * @generated NOT
	 * Tests UC 2.2.6
	 */
	public void testRemoveRoom__int() {
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoom(100, "foo");
		fixture.addRoom(200, "foo");

		assertEquals(2, fixture.getRooms().size());
		fixture.removeRoom(100);
		assertEquals(1, fixture.getRooms().size());
		fixture.removeRoom(200);
		assertEquals(0, fixture.getRooms().size());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#blockRoom(int) <em>Block Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#blockRoom(int)
	 * @generated NOT
	 * Tests UC 2.2.7
	 */
	public void testBlockRoom__int() {
		assertEquals(0, fixture.getBlockedRooms().size());
		
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoom(100, "foo");

		fixture.blockRoom(100);
		assertEquals(1, fixture.getBlockedRooms().size());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#unblockRoom(int) <em>Unblock Room</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#unblockRoom(int)
	 * @generated NOT
	 * Tests UC 2.2.8
	 */
	public void testUnblockRoom__int() {
		
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoom(100, "foo");

		fixture.blockRoom(100);
		assertEquals(1, fixture.getBlockedRooms().size());
		fixture.unblockRoom(100);
		assertEquals(0, fixture.getBlockedRooms().size());

	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#isRoomBlocked(int) <em>Is Room Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#isRoomBlocked(int)
	 * @generated NOT
	 */
	public void testIsRoomBlocked__int() {
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoom(100, "foo");
		
		assertEquals(false, fixture.isRoomBlocked(100));
		fixture.blockRoom(100);		
		assertEquals(true, fixture.isRoomBlocked(100));
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#clearRooms() <em>Clear Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomManagerProvides#clearRooms()
	 * @generated NOT
	 */
	public void testClearRooms() {
		fixture.addRoomType("foo", 10, 11, "bay");
		fixture.addRoom(100, "foo");
		fixture.blockRoom(100);

		assertEquals(1, fixture.getRooms().size());
		assertEquals(1, fixture.getRoomTypes().size());
		assertEquals(1, fixture.getBlockedRooms().size());
		
		fixture.clearRooms();
		
		assertEquals(0, fixture.getRooms().size());
		assertEquals(0, fixture.getRoomTypes().size());
		assertEquals(0, fixture.getBlockedRooms().size());
	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRooms() <em>Get All Rooms</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRooms()
	 * @generated NOT
	 */
	public void testGetAllRooms() {
		fixture.addRoomType("bar", 12, 13, "baz");

		fixture.addRoom(100, "bar");
		fixture.addRoom(200, "bar");
		
		assertEquals(2, fixture.getAllRooms().size());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#isRoomBlocked(se.chalmers.cse.mdsd1617.group17.Room) <em>Is Room Blocked</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#isRoomBlocked(se.chalmers.cse.mdsd1617.group17.Room)
	 * @generated NOT
	 */
	public void testIsRoomBlocked__Room() {
		fixture.addRoomType("bar", 12, 13, "baz");
		fixture.addRoom(100, "bar");
		Room room = fixture.getRooms().get(0);
		
		assertEquals(false, fixture.isRoomBlocked(room));
		fixture.blockRoom(100);
		assertEquals(true, fixture.isRoomBlocked(room));
		
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRoomTypes() <em>Get All Room Types</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IRoomAccessProvides#getAllRoomTypes()
	 * @generated NOT
	 */
	public void testGetAllRoomTypes() {
		RoomManager rm = new RoomManagerImpl();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoomType("B", 20, 1, "tex-mex");
		rm.addRoomType("C", 5, 5, "tex");
		
		assertEquals(rm.getAllRoomTypes().size(), 3);
		assertEquals(rm.getRoomTypes().get(0).getName(), "A");
		assertEquals(rm.getRoomTypes().get(1).getName(), "B");
		assertEquals(rm.getRoomTypes().get(2).getName(), "C");
	}

} //RoomManagerTest
