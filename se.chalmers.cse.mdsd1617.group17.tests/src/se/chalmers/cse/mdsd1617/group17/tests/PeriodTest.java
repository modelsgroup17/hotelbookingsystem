/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import junit.framework.TestCase;

import junit.textui.TestRunner;

import se.chalmers.cse.mdsd1617.group17.Group17Factory;
import se.chalmers.cse.mdsd1617.group17.Period;
import se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Period</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period) <em>Overlaps With</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.Period#isDateInPeriod(java.util.Date) <em>Is Date In Period</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class PeriodTest extends TestCase {

	/**
	 * The fixture for this Period test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Period fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(PeriodTest.class);
	}

	/**
	 * Constructs a new Period test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PeriodTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Period test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Period fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Period test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Period getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Group17Factory.eINSTANCE.createPeriod());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}
	
	private Date createDateFromString(String date) {
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		try {
			return dateFormat.parse(date);
		} catch (ParseException e) {
			throw new IllegalArgumentException();
		}
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period) <em>Overlaps With</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period)
	 * @generated NOT
	 */
	public void testOverlapsWith__Period() throws ParseException {
		Period p1 = new PeriodImpl(createDateFromString("20161201"), createDateFromString("20161205"));
		Period p2 = new PeriodImpl(createDateFromString("20161205"), createDateFromString("20161206"));
		Period p3 = new PeriodImpl(createDateFromString("20161207"), createDateFromString("20161208"));
		
		assertTrue(p1.overlapsWith(p2));
		assertTrue(p2.overlapsWith(p1));
		
		assertFalse(p1.overlapsWith(p3));
		assertFalse(p3.overlapsWith(p1));
		
		assertFalse(p2.overlapsWith(p3));
		assertFalse(p3.overlapsWith(p2));
	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.Period#isDateInPeriod(java.util.Date) <em>Is Date In Period</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.Period#isDateInPeriod(java.util.Date)
	 * @generated NOT
	 */
	public void testIsDateInPeriod__Date() {
		try {
			Period p1 = new PeriodImpl("20161202", "20161205");
			Period p2 = new PeriodImpl("20161204", "20161205");
			Date d = createDateFromString("20161202");
			if (p1.isDateInPeriod(d) && !p2.isDateInPeriod(d)) {
				// Success
			} else {
				fail();
			}
		} catch (Exception e) {
			e.printStackTrace();
			fail("some error");
		}
	}

	/**
	 * @see se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period)
	 * @generated NOT
	 * @Test(expected=IllegalArgumentException.class)
	 */
	public void testStartGreaterThanEnd() {
		try {
			new PeriodImpl(createDateFromString("20161202"), createDateFromString("20161201"));
			fail("Was excepting IllegalArgumentException");
		} catch (Exception e) {
			// Successfully done
		}
	}
	/**
	 * @see se.chalmers.cse.mdsd1617.group17.Period#overlapsWith(se.chalmers.cse.mdsd1617.group17.Period)
	 * @generated NOT
	 * @Test(expected=ParseException.class)
	 */
	public void testParseInvalidDate() {
		try {
			new PeriodImpl("2016-01-01", "2016-12-01");
			fail("Was excepting ParseException");
		} catch (ParseException e) {
			// Successfully done
		}
	}

} //PeriodTest
