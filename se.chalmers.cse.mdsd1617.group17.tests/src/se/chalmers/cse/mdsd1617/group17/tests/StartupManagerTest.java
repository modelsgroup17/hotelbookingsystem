/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import java.text.ParseException;

import junit.framework.TestCase;

import junit.textui.TestRunner;
import se.chalmers.cse.mdsd1617.group17.BookingManager;
import se.chalmers.cse.mdsd1617.group17.Group17Factory;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomManager;
import se.chalmers.cse.mdsd1617.group17.StartupManager;
import se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl;
import se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl;
import se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Startup Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides#startup(int) <em>Startup</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class StartupManagerTest extends TestCase {

	/**
	 * The fixture for this Startup Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StartupManager fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(StartupManagerTest.class);
	}

	/**
	 * Constructs a new Startup Manager test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StartupManagerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Startup Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(StartupManager fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Startup Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StartupManager getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Group17Factory.eINSTANCE.createStartupManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides#startup(int) <em>Startup</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelStartupProvides#startup(int)
	 * @generated NOT
	 * Tests UC 2.2.9
	 */
	public void testStartup__int() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		RoomManager roommanager = sm.getRoommanager();
		BookingManagerImpl bookingmanager = (BookingManagerImpl) sm.getBookingmanager();
			
		assertNotNull(bookingmanager);
		assertNotNull(roommanager);
		
		roommanager.clearRooms();
		bookingmanager.clearBookings();
		bookingmanager.getBookings();
		
		assertEquals(0, roommanager.getAllRoomTypes().size());
		assertEquals(0, bookingmanager.getBookings().size());
		assertEquals(0, roommanager.getAllRooms().size());
		
		sm.startup(4);
		assertEquals(4, roommanager.getAllRooms().size());
		for (Room r : roommanager.getAllRooms()) {
			assertEquals(2, r.getRoomType().getNumBeds());
		}
		assertEquals("default", roommanager.getAllRoomTypes().get(0).getName());
		try {
			assertEquals(4, bookingmanager.getFreeRoomsList(2, new PeriodImpl("20160101", "20160102")).size());
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail();
		}
	}

} //StartupManagerTest
