/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>group17</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Group17Tests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Group17Tests("group17 Tests");
		suite.addTestSuite(PeriodTest.class);
		suite.addTestSuite(BookingManagerTest.class);
		suite.addTestSuite(RoomManagerTest.class);
		suite.addTestSuite(StartupManagerTest.class);
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Group17Tests(String name) {
		super(name);
	}

} //Group17Tests
