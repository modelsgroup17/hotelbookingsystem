/**
 */
package se.chalmers.cse.mdsd1617.group17.tests;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.soap.SOAPException;
import org.eclipse.emf.common.util.EList;
import junit.framework.TestCase;

import junit.textui.TestRunner;
import se.chalmers.cse.mdsd1617.banking.administratorRequires.AdministratorRequires;
import se.chalmers.cse.mdsd1617.group17.Booking;
import se.chalmers.cse.mdsd1617.group17.BookingManager;
import se.chalmers.cse.mdsd1617.group17.FreeRoomTypesDTO;
import se.chalmers.cse.mdsd1617.group17.Group17Factory;
import se.chalmers.cse.mdsd1617.group17.Period;
import se.chalmers.cse.mdsd1617.group17.Reservation;
import se.chalmers.cse.mdsd1617.group17.ReservationStatus;
import se.chalmers.cse.mdsd1617.group17.Room;
import se.chalmers.cse.mdsd1617.group17.RoomManager;
import se.chalmers.cse.mdsd1617.group17.RoomType;
import se.chalmers.cse.mdsd1617.group17.StartupManager;
import se.chalmers.cse.mdsd1617.group17.impl.BookingImpl;
import se.chalmers.cse.mdsd1617.group17.impl.BookingManagerImpl;
import se.chalmers.cse.mdsd1617.group17.impl.Group17FactoryImpl;
import se.chalmers.cse.mdsd1617.group17.impl.PeriodImpl;
import se.chalmers.cse.mdsd1617.group17.impl.ReservationImpl;
import se.chalmers.cse.mdsd1617.group17.impl.RoomImpl;
import se.chalmers.cse.mdsd1617.group17.impl.RoomManagerImpl;
import se.chalmers.cse.mdsd1617.group17.impl.RoomTypeImpl;



/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Booking Manager</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following operations are tested:
 * <ul>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#editABooking(int, se.chalmers.cse.mdsd1617.group17.RoomType, java.util.Date, java.util.Date, int) <em>Edit ABooking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#cancelABooking(int) <em>Cancel ABooking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listBookings() <em>List Bookings</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#occupiedRoomsForDate(java.util.Date) <em>Occupied Rooms For Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date) <em>List Check Ins For Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date) <em>List Check Outs For Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#addExtraCostsForRoom(int, int, java.lang.String, double) <em>Add Extra Costs For Room</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#checkInBooking(int) <em>Check In Booking</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date, java.util.Date) <em>List Check Ins For Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date, java.util.Date) <em>List Check Outs For Date</em>}</li>
 *   <li>{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#clearBookings() <em>Clear Bookings</em>}</li>
 * </ul>
 * </p>
 * @generated
 */
public class BookingManagerTest extends TestCase {

	/**
	 * The fixture for this Booking Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingManager fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(BookingManagerTest.class);
	}

	/**
	 * Constructs a new Booking Manager test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BookingManagerTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Booking Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated NOT
	 */
	protected void setFixture(BookingManager fixture) {
		this.fixture = fixture;
		if (fixture != null) {
			fixture.clearBookings();
			RoomManager rm = (RoomManager) fixture.getIroomavailabilitymanagerprovides();
			rm.clearRooms();
		}
	}

	/**
	 * Returns the fixture for this Booking Manager test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BookingManager getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Group17Factory.eINSTANCE.createBookingManager());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String) <em>Initiate Booking</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateBooking(java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 * @generated NOT
	 * UC 2.1.1 - MAKE A BOOKING
	 * This test makes sure that a booking i created in the right way
	 */
	public void testInitiateBooking__String_String_String_String() throws ParseException {

		// get -1 if the first name is empty
		assertEquals(-1, fixture.initiateBooking("", "20161217", "1812-20161218", "Indiana"));
	
		// get -1 if the last name is empty
		assertEquals(-1, fixture.initiateBooking("Jones", "20160101", "20160101", ""));
		
		// get the booking id if the names are not empty and date input is correct
		assertEquals(1, fixture.initiateBooking("Rutger", "20160101", "20160101", "Andersson"));

		// get -1 if the start date is grater than end date
		assertEquals(-1, fixture.initiateBooking("Sylvester", "20161222", "20161220", "Stallone"));

		// get 2 if the start date and end date is the same
		assertEquals(2, fixture.initiateBooking("James", "20160101", "20160101", "Bond"));

		// get -1 if the date format is invalid
		assertEquals(-1, fixture.initiateBooking("James", "20-1211-2016", "20-12222-2016", "Bond"));
	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int) <em>Add Room To Booking</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#addRoomToBooking(java.lang.String, int)
	 * @generated NOT
	 * UC 2.1.1 - MAKE A BOOKING
	 * checks if a reservation can be added to a booking
	 */
	public void testAddRoomToBooking__String_int() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		BookingManager bm = sm.getBookingmanager();
		RoomManager rm = sm.getRoommanager();
		
		bm.clearBookings();
		rm.clearRooms();

		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		
		int bID = bm.initiateBooking("Alfons", "20160101", "20160101", "Åberg");
		
		EList<Reservation> reservations = bm.getBookings().get(0).getReservations();
		
		// No reservation added yet
		assertTrue(reservations.isEmpty());
		
		// Try requesting an invalid RoomType, no reservation should be made
		bm.addRoomToBooking("B", bID);
		assertTrue(reservations.isEmpty());
		
		// A reservation of type "A" should have been made
		bm.addRoomToBooking("A", bID);
		assertEquals(1, reservations.size());
		assertTrue(reservations.get(0).getRoomType().getName().equals("A"));
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#confirmBooking(int) <em>Confirm Booking</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#confirmBooking(int)
	 * @generated NOT
	 * UC 2.1.1 - MAKE A BOOKING
	 * checks if the booking can be confirmed
	 */
	public void testConfirmBooking__int() {
		RoomManager rm = (RoomManager) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		
		int bookingID = fixture.initiateBooking("Bill", "20160101", "20160101", "Gates");
		
		// No reservations yet.
		assertEquals(0,fixture.getBookings().get(0).getReservations().size());
		
		// One reservation.
		fixture.addRoomToBooking("A", bookingID);
		
		// Reservation is marked "created".
		Reservation res = fixture.getBookings().get(0).getReservations().get(0);
		assertEquals(ReservationStatus.CREATED, res.getStatus());
		
		// Reservation is marked "confirmed".
		fixture.confirmBooking(bookingID);
		assertEquals(ReservationStatus.CONFIRMED, res.getStatus());
	}


	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String) <em>Get Free Rooms</em>}' operation.
	  * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#getFreeRooms(int, java.lang.String, java.lang.String)
	 * @generated NOT
	 *  * UC 2.1.2 - SEARCH FOR FREE ROOMS
	 */
	public void testGetFreeRooms__int_String_String() {
		RoomManager roomManager = Group17Factory.eINSTANCE.createRoomManager();
		roomManager.addRoomType("double-room", 12, 2, "tv");
		roomManager.addRoom(1, "double-room");
		
		int b1 = fixture.initiateBooking("John", "20160101", "20160102", "Doe");
		int b2 = fixture.initiateBooking("Johny", "20160101", "20160102", "Doey");
		int b3 = fixture.initiateBooking("Johny", "20160103", "20160104", "Doeya");
		
		assertTrue(fixture.addRoomToBooking("double-room", b1));		
		assertFalse(fixture.addRoomToBooking("double-room", b2));
		assertTrue(fixture.addRoomToBooking("double-room", b3));		
		
		assertEquals(0, fixture.getFreeRooms(2, "20160101", "20160104").size());
		assertEquals(1, fixture.getFreeRooms(2, "20160105", "20160105").size());
		
		roomManager.addRoomType("double-room", 12, 2, "tv");
		roomManager.addRoom(4, "double-room");
		roomManager.addRoom(2, "double-room");
		roomManager.addRoom(3, "double-room");
		
		EList<FreeRoomTypesDTO> dto = fixture.getFreeRooms(2, "20160101", "20160104");
		assertEquals(1, dto.size());
		assertEquals("double-room", dto.get(0).getRoomTypeDescription());
		assertEquals(2, dto.get(0).getNumFreeRooms());
		
		int b4 = fixture.initiateBooking("John", "20160101", "20160102", "Doe");
		assertTrue(fixture.addRoomToBooking("double-room", b4));		

		dto = fixture.getFreeRooms(2, "20160101", "20160104");
		assertEquals(1, dto.size());
		assertEquals("double-room", dto.get(0).getRoomTypeDescription());
		assertEquals(1, dto.get(0).getNumFreeRooms());

		assertTrue(fixture.addRoomToBooking("double-room", b4));		
		assertTrue(fixture.addRoomToBooking("double-room", b4));		

		dto = fixture.getFreeRooms(2, "20160101", "20160104");
		assertTrue(dto.isEmpty());
	}
	

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#checkInRoom(java.lang.String, int) <em>Check In Room</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#checkInRoom(java.lang.String, int)
	 * @generated NOT
	 * Tests UC 2.1.11
	 */
	public void testCheckInRoom__String_int() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		RoomManager roomManager = sm.getRoommanager();
		
		roomManager.addRoomType("suite", 70, 3,"garden view");
		roomManager.addRoom(3, "suite");
		roomManager.addRoom(4, "suite");

		int id1 = fixture.initiateBooking("Ashley", "20161115", "20161115", "Key");
		int id2 = fixture.initiateBooking("Ayy", "20161115", "20161115", "Lamoooo");
		
		fixture.addRoomToBooking("suite", id1);
		fixture.confirmBooking(id1);
		assertFalse(roomManager.isRoomBlocked(3));
		
		// Try checking in room that should work
		assertEquals(3, fixture.checkInRoom("suite", id1));
		
		// Try checking in room that shouldn't work
		assertEquals(-1, fixture.checkInRoom("suite", id2));
		roomManager.blockRoom(4);
	}


	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateCheckout(int) <em>Initiate Checkout</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateCheckout(int)
	 * @generated NOT
	 * UC 2.1.4 - CHECK OUT BOOKING
	 */
	public void testInitiateCheckout__int() throws ParseException {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		BookingManager bm = sm.getBookingmanager();
		RoomManager roomManager = sm.getRoommanager();

		//add room types
		roomManager.addRoomType("svit", 1500, 2, "ocean view");
		roomManager.addRoomType("double room", 800, 2, "has a bathroom");

		//add rooms
		roomManager.addRoom(2, "svit");
		roomManager.addRoom(3, "double room");
		roomManager.addRoom(4, "svit");
		roomManager.addRoom(5, "svit");
		roomManager.addRoom(6, "svit");
		roomManager.addRoom(7, "svit");
		
		try {
			// Payment is not tested here -- but we need to pay to checkout next to release the lock
			AdministratorRequires bankingAdmin = sm.getRoommanager().getBankingAdmin();
			
			bankingAdmin.addCreditCard("17938583838591938", "123", 10, 20, "A", "B");
			bankingAdmin.makeDeposit("17938583838591938", "123", 10, 20, "A", "B", 99999999999d);
			
			bankingAdmin.addCreditCard("1744444433333333", "123", 10, 20, "Kalle", "Andersson");
			bankingAdmin.makeDeposit("1744444433333333", "123", 10, 20, "Kalle", "Andersson", 100000000);

		} catch (SOAPException e) {
			fail("should be able to add credit card");
		}

		int bookingID1 = bm.initiateBooking("A", "20161111", "20161119", "B");
		assertTrue(bm.addRoomToBooking("svit", bookingID1));
		assertTrue(bm.addRoomToBooking("svit", bookingID1));
		assertEquals(1, bm.getBookings().size());
		assertEquals(2, bm.getBookings().get(0).getReservations().size());
		bm.confirmBooking(bookingID1);

		// Not yet checked in
		assertTrue(bm.initiateCheckout(bookingID1) == 0d);
		bm.checkInBooking(bookingID1);
		//Just add a room without any extra costs
		assertEquals(3000.0 , bm.initiateCheckout(bookingID1));
		assertTrue(bm.payDuringCheckout("17938583838591938", "123", 10, 20, "A", "B"));

		
		//Add two room with extra costs for one room
		int bookingID3 = fixture.initiateBooking("Kalle", "20160611", "20160719", "Andersson");
		fixture.addRoomToBooking("svit", bookingID3);
		fixture.addRoomToBooking("svit", bookingID3);
		fixture.confirmBooking(bookingID3);

		Reservation finalReservation3 = new ReservationImpl();
		for(Booking b: fixture.getBookings()) {
			if(b.getBookingID() == bookingID3) {
				finalReservation3 = b.getReservations().get(0);
			}
		}
		assertEquals(null ,  finalReservation3.getRoom());
		fixture.checkInBooking(bookingID3);
		fixture.addExtraCostsForRoom(bookingID3, finalReservation3.getReservationID(), "ice cream", 25);
		assertEquals(3025.0 , fixture.initiateCheckout(bookingID3));

	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay During Checkout</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payDuringCheckout(java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated NOT
	 * UC 2.1.4 - CHECK OUT BOOKING
	 */
	public void testPayDuringCheckout__String_String_int_int_String_String() {
		int bookingId = fixture.initiateBooking("John", "20160101", "20160104", "Doe");
		RoomManagerImpl rm = (RoomManagerImpl) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		//Assert False since the booking is not checked in
		assertFalse(fixture.payDuringCheckout("1717171717171717", "171", 3, 17, "John", "Doe"));
		fixture.addRoomToBooking("A", bookingId);
		fixture.confirmBooking(bookingId);
		fixture.checkInBooking(bookingId);
		Reservation res = fixture.getBookings().get(0).getReservations().get(0);		
		fixture.addExtraCostsForRoom(bookingId, res.getReservationID(), "Coffe", 100.0);
		//Should assert false since no credit card in the system
		assertFalse(fixture.payDuringCheckout("1717171717171718", "178", 3, 17, "John", "Doe"));
		
		try{			
			rm.getBankingAdmin().addCreditCard("1756756756756756", "171", 3, 17, "John", "Doe");
			rm.getBankingAdmin().makeDeposit("1756756756756756", "171", 3, 17, "John", "Doe", 1000000.0);
		}catch (SOAPException e) {
			
			e.printStackTrace();
		}
		fixture.initiateCheckout(bookingId);
		assertTrue(fixture.payDuringCheckout("1756756756756756", "171", 3, 17, "John", "Doe"));		
	}


	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateRoomCheckout(int, int) <em>Initiate Room Checkout</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#initiateRoomCheckout(int, int)
	 * @generated NOT
	 * Tests UC 2.1.12
	 */
	public void testInitiateRoomCheckout__int_int() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();

		try {
			AdministratorRequires bankingAdmin = sm.getRoommanager().getBankingAdmin();
			// card for first booking
			bankingAdmin.addCreditCard("1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig");
			bankingAdmin.makeDeposit("1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig", 1300000000000.4);
			
			// card for second booking
			bankingAdmin.addCreditCard("1721111455667788", "123", 10, 20, "Josef", "Kerschischnig");
			bankingAdmin.makeDeposit("1721111455667788", "123", 10, 20, "Josef", "Kerschischnig", 1300000000000.4);
			
		} catch (SOAPException e) {
			fail("should be able to add credit card");
		}
		
		BookingManager bm = sm.getBookingmanager();
		sm.startup(16);
		
		int bookingID1 = bm.initiateBooking("Marcus", "20170225", "20170325", "Kerschischnig");
		int bookingID2 = bm.initiateBooking("Josef", "20170225", "20170325", "Kerschischnig");
		
		bm.addRoomToBooking("default", bookingID1);
		bm.addRoomToBooking("default", bookingID1);
		
		bm.addRoomToBooking("default", bookingID2);
		bm.addRoomToBooking("default", bookingID2);
		bm.addRoomToBooking("default", bookingID2);
		
		bm.confirmBooking(bookingID1);
		bm.confirmBooking(bookingID2);
		
		int room1 = bm.checkInRoom("default", bookingID1);
		int room2 = bm.checkInRoom("default", bookingID1);
		
		int room3 = bm.checkInRoom("default", bookingID2);
		int room4 = bm.checkInRoom("default", bookingID2);
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room1, bookingID1));
		
		assertTrue(bm.payRoomDuringCheckout(room1, "1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig"));
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room2, bookingID1));
		assertTrue(bm.payRoomDuringCheckout(room2, "1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig"));
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room3, bookingID2));
		assertTrue(bm.payRoomDuringCheckout(room3, "1721111455667788", "123", 10, 20, "Josef", "Kerschischnig"));

		assertEquals(1200.0, bm.initiateRoomCheckout(room4, bookingID2));
		assertTrue(bm.payRoomDuringCheckout(room4, "1721111455667788", "123", 10, 20, "Josef", "Kerschischnig"));
	}
	
	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String) <em>Pay Room During Checkout</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IHotelCustomerProvides#payRoomDuringCheckout(int, java.lang.String, java.lang.String, int, int, java.lang.String, java.lang.String)
	 * @generated NOT
	 * Tests UC 2.1.12
	 */
	public void testPayRoomDuringCheckout__int_String_String_int_int_String_String() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		BookingManager bm = sm.getBookingmanager();
		sm.startup(16);
		
		int bookingID1 = bm.initiateBooking("Elin", "20170225", "20170325", "Pelin");
		int bookingID2 = bm.initiateBooking("Emil", "20170225", "20170325", "Hansson");
		
		bm.addRoomToBooking("default", bookingID1);
		bm.addRoomToBooking("default", bookingID1);
		
		bm.addRoomToBooking("default", bookingID2);
		bm.addRoomToBooking("default", bookingID2);
		bm.addRoomToBooking("default", bookingID2);
		
		bm.confirmBooking(bookingID1);
		bm.confirmBooking(bookingID2);
		
		int room1 = bm.checkInRoom("default", bookingID1);
		int room2 = bm.checkInRoom("default", bookingID1);
		
		int room3 = bm.checkInRoom("default", bookingID2);
		int room4 = bm.checkInRoom("default", bookingID2);
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room1, bookingID1));
		
		try {
			AdministratorRequires bankingAdmin = sm.getRoommanager().getBankingAdmin();
			// card for first booking
			bankingAdmin.addCreditCard("1722334455667781", "123", 10, 20, "Elin", "Pelin");
			bankingAdmin.makeDeposit("1722334455667781", "123", 10, 20, "Elin", "Pelin", 1300000000000.4);
			
			// card for second booking
			bankingAdmin.addCreditCard("1722111155667781", "222", 10, 20, "Emil", "Hansson");
			bankingAdmin.makeDeposit("1722111155667781", "222", 10, 20, "Emil", "Hansson", 1300000000000.4);

			
		} catch (SOAPException e) {
			fail("should be able to add credit card");
		}
		assertTrue(bm.payRoomDuringCheckout(room1, "1722334455667781", "123", 10, 20, "Elin", "Pelin"));
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room2, bookingID1));
		//should fail, wrong name
		assertFalse(bm.payRoomDuringCheckout(room2, "1722334455667781", "123", 10, 20, "Daniel", "Karlsson"));
		assertTrue(bm.payRoomDuringCheckout(room2, "1722334455667781", "123", 10, 20, "Elin", "Pelin"));
		
		assertEquals(1200.0, bm.initiateRoomCheckout(room3, bookingID2));
		assertTrue(bm.payRoomDuringCheckout(room3, "1722111155667781", "222", 10, 20, "Emil", "Hansson"));

		assertEquals(1200.0, bm.initiateRoomCheckout(room4, bookingID2));
		assertTrue(bm.payRoomDuringCheckout(room4, "1722111155667781", "222", 10, 20, "Emil", "Hansson"));
	}

	
	/**
	 * Ensures combinations of check out specific room / all and pay for specific and pay for all works.
	 */
	public void testCheckoutPayIndividualAndAll() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		sm.startup(16);

		try {
			AdministratorRequires bankingAdmin = sm.getRoommanager().getBankingAdmin();
			// card for first booking
			bankingAdmin.addCreditCard("1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig");
			bankingAdmin.makeDeposit("1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig", 1300000000000.4);
			
			// card for second booking
			bankingAdmin.addCreditCard("1721111455667788", "123", 10, 20, "Josef", "Kerschischnig");
			bankingAdmin.makeDeposit("1721111455667788", "123", 10, 20, "Josef", "Kerschischnig", 1300000000000.4);
			
		} catch (SOAPException e) {
			fail("should be able to add credit card");
		}
		
		BookingManager bm = sm.getBookingmanager();
		
		int b1 = bm.initiateBooking("Marcus", "20170225", "20170325", "Kerschischnig");
		
		bm.addRoomToBooking("default", b1);
		bm.addRoomToBooking("default", b1);
		bm.addRoomToBooking("default", b1);
		
		// This room will not be checked in before the first 3 has been payed
		bm.addRoomToBooking("default", b1);
		
		bm.confirmBooking(b1);
		
		int r1 = bm.checkInRoom("default", b1);
		int r2 = bm.checkInRoom("default", b1);
		int r3 = bm.checkInRoom("default", b1);
		
		// Check out + pay for first
		assertEquals(1200.0, bm.initiateRoomCheckout(r1, b1));
		assertTrue(bm.payRoomDuringCheckout(r1, "1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig"));
		
		// Check out second, third
		assertEquals(2400.0, bm.initiateCheckout(b1));
		assertTrue(bm.payDuringCheckout("1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig"));
		
		// Check in, out and pay for fourth
		int r4 = bm.checkInRoom("default", b1);
		assertEquals(1200.0, bm.initiateCheckout(b1));
		assertTrue(bm.payRoomDuringCheckout(r4,"1722334455667788", "123", 10, 20, "Marcus", "Kerschischnig"));
	}
	
	/**
	 * Ensures that no lock is applied when checking out one room at a time.
	 * Tests UC 2.1.12
	 */
	public void testCheckoutRoomParalell() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		sm.startup(4);

		try {
			AdministratorRequires bankingAdmin = sm.getRoommanager().getBankingAdmin();
			// card for first booking
			bankingAdmin.addCreditCard("1722334455661111", "123", 10, 20, "Marcus", "Kerschischnig");
			bankingAdmin.makeDeposit("1722334455661111", "123", 10, 20, "Marcus", "Kerschischnig", 1300000000000.4);
			
			// card for second booking
			bankingAdmin.addCreditCard("1721111455662222", "123", 10, 20, "Josef", "Kerschischnig");
			bankingAdmin.makeDeposit("1721111455662222", "123", 10, 20, "Josef", "Kerschischnig", 1300000000000.4);
			
		} catch (SOAPException e) {
			fail("should be able to add credit card");
		}
		
		BookingManager bm = sm.getBookingmanager();
		
		int b1 = bm.initiateBooking("Foo", "20170225", "20170325", "Bar");
		int b2 = bm.initiateBooking("Fooy", "20170225", "20170325", "Bary");
		
		assertTrue(bm.addRoomToBooking("default", b1));
		assertTrue(bm.addRoomToBooking("default", b1));

		assertTrue(bm.addRoomToBooking("default", b2));
		assertTrue(bm.addRoomToBooking("default", b2));
		
		assertTrue(bm.confirmBooking(b1));
		assertTrue(bm.confirmBooking(b2));

		int r11 = bm.checkInRoom("default", b1);
		int r12 = bm.checkInRoom("default", b1);

		int r21 = bm.checkInRoom("default", b2);
		int r22 = bm.checkInRoom("default", b2);
		
		assertEquals(1200.0, bm.initiateRoomCheckout(r11, b1)); // No lock should be applied here as we check out one room only
		assertEquals(1200.0, bm.initiateRoomCheckout(r12, b1));
		
		assertEquals(1200.0, bm.initiateRoomCheckout(r21, b2));
		assertEquals(1200.0, bm.initiateRoomCheckout(r22, b2));
	}
	

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#editABooking(int, se.chalmers.cse.mdsd1617.group17.RoomType, java.util.Date, java.util.Date, int) <em>Edit ABooking</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#editABooking(int, se.chalmers.cse.mdsd1617.group17.RoomType, java.util.Date, java.util.Date, int)
	 * @generated NOT
	 * UC 2.1.5 - EDIT A BOOKING
	 */
	public void testEditABooking__int_RoomType_Date_Date_int() throws ParseException {
		
		RoomManager roomManager = (RoomManager)fixture.getIroomavailabilitymanagerprovides();
		
		//add room types
		roomManager.addRoomType("svit", 1500, 2, "ocean view");
		roomManager.addRoomType("double room", 800, 2, "has a bathroom");
		
		//add rooms
		roomManager.addRoom(2, "svit");
		roomManager.addRoom(3, "double room");
		roomManager.addRoom(4, "double room");
		roomManager.addRoom(5, "double room");
		
		//add booking
		int bookingID = fixture.initiateBooking("Elin", "20161111", "20161119", "Bjornsson");
		fixture.addRoomToBooking("svit", bookingID);
		fixture.confirmBooking(bookingID);
		
		Period p1 = new PeriodImpl("20161111", "20161119");
		Period p2 = new PeriodImpl("20171111", "20171119");
		
		Reservation finalReservation = new ReservationImpl();
		for(Booking b: fixture.getBookings()) {
			if(b.getBookingID() == bookingID) {
				finalReservation = b.getReservations().get(0);
			}
		}
		
		assertEquals("svit" ,  finalReservation.getRoomType().getName());
		assertTrue(p1.equals(finalReservation.getPeriod()));
		assertEquals(1, fixture.getBookings().get(0).getReservations().size());
		
		fixture.editABooking(bookingID, roomManager.getRoomTypes().get(1), p2.getStart(), p2.getEnd(), 3);
		
		for(Booking b: fixture.getBookings()) {
			if(b.getBookingID() == bookingID) {
				finalReservation = b.getReservations().get(0);
			}
		}
		
		assertEquals(3, fixture.getBookings().get(0).getReservations().size());
		assertTrue(p2.equals(finalReservation.getPeriod()));
		
		//check if it is the correct room type
		assertEquals(roomManager.getAllRoomTypes().get(1), finalReservation.getRoomType());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#cancelABooking(int) <em>Cancel ABooking</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#cancelABooking(int)
	 * @generated NOT
	 * Tests UC 2.1.6
	 */
	public void testCancelABooking__int() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		BookingManager bm = sm.getBookingmanager();
		int bookingID = bm.initiateBooking("Mike", "20160101", "20160101", "Tyson");
		assertEquals(1, bm.getBookings().size());
		bm.cancelABooking(bookingID);
		assertEquals(0, bm.getBookings().size());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listBookings() <em>List Bookings</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listBookings()
	 * @generated NOT
	 * Tests UC 2.1.7
	 */
	public void testListBookings() {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		BookingManager bm = sm.getBookingmanager();
		sm.startup(2);

		assertEquals(0, fixture.listBookings().size());

		
		int b1 = bm.initiateBooking("Roberta", "20160101", "20160102", "DeC");
		int b2 = bm.initiateBooking("Foo", "20160101", "20160102", "DeC");
		int b3 = bm.initiateBooking("Bar", "20160103", "20160104", "DeC");

		assertTrue(bm.addRoomToBooking("default", b1));
		assertTrue(bm.confirmBooking(b1));
		assertEquals(1, bm.listBookings().size());

		assertTrue(bm.addRoomToBooking("default", b2));
		assertTrue(bm.confirmBooking(b2));
		assertEquals(2, bm.listBookings().size());
		
		assertTrue(bm.addRoomToBooking("default", b3));
		assertEquals(2, bm.listBookings().size());
		assertTrue(bm.confirmBooking(b3));
		assertEquals(3, bm.listBookings().size());

		bm.checkInBooking(b1);
		assertEquals(2, bm.listBookings().size());
		
		bm.checkInBooking(b2);
		assertEquals(1, bm.listBookings().size());

		bm.checkInBooking(b3);
		assertEquals(0, bm.listBookings().size());
		
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#occupiedRoomsForDate(java.util.Date) <em>Occupied Rooms For Date</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#occupiedRoomsForDate(java.util.Date)
	 * @generated NOT
	 * Tests UC 2.1.8
	 */
	public void testOccupiedRoomsForDate__Date() throws ParseException {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		sm.startup(3);
		BookingManager bm = sm.getBookingmanager();
				
		int bID1 = bm.initiateBooking("Mike", "20160101", "20160101", "Bike");
		int bID2 = bm.initiateBooking("Anders", "20160101", "20160101", "Bikesson");
		
		try {
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			Date d = df.parse("20160101");
			
			// No room should be occupied yet
			assertEquals(0, bm.occupiedRoomsForDate(d).size());
			
			// One room should be occupied
			bm.addRoomToBooking("default", bID1);
			assertEquals(1, bm.occupiedRoomsForDate(d).size());
			
			// Two rooms should be occupied
			bm.addRoomToBooking("default", bID2);
			assertEquals(2, bm.occupiedRoomsForDate(d).size());
			
			// Cancel a booking and check that only one room occupies that date
			bm.cancelABooking(bID1);
			assertEquals(1, bm.occupiedRoomsForDate(d).size());
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date) <em>List Check Ins For Date</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date)
	 * @generated NOT
	 * Tests UC 2.1.9
	 */
	public void testListCheckInsForDate__Date() {
		//Basic test, one check in during one date should assert true
		Period per = null;
		try {
			per = new PeriodImpl("20160101","20160104");
		} catch (Exception e) {
			e.printStackTrace();
		}
		//No bookings made so should be no check ins on given date
		assertTrue(fixture.listCheckInsForDate(per.getStart()).isEmpty());
		
		int bookingId = fixture.initiateBooking("John", "20160101", "20160104", "Doe");
		RoomManagerImpl rm = (RoomManagerImpl) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		fixture.addRoomToBooking("A", bookingId);
		fixture.confirmBooking(bookingId);

		for(Booking booking : fixture.listBookings()){
			if(booking.getBookingID()== bookingId){
				assertTrue(fixture.listCheckInsForDate(per.getStart()).contains(booking));
			}
		}
		fixture.cancelABooking(bookingId);
		//Removed booking so should be no check ins during given date
		assertTrue(fixture.listCheckInsForDate(per.getStart()).isEmpty());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date) <em>List Check Outs For Date</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date)
	 * @generated NOT
	 * Tests UC 2.1.10
	 */
	public void testListCheckOutsForDate__Date() throws ParseException {
		//Basic test, one check out during one date should assert true
		Period per = new PeriodImpl("20160101","20160104");
		
		//No bookings made so should be no check outs on given date
		assertTrue(fixture.listCheckOutsForDate(per.getEnd()).isEmpty());

		int bookingId = fixture.initiateBooking("John", "20160101", "20160104", "Doe");
		RoomManagerImpl rm = (RoomManagerImpl) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		fixture.addRoomToBooking("A", bookingId);
		fixture.confirmBooking(bookingId);

		for(Booking booking : fixture.listBookings()){
			if(booking.getBookingID()== bookingId){
				assertTrue(fixture.listCheckOutsForDate(per.getEnd()).contains(booking));
			}
		}
		fixture.cancelABooking(bookingId);
		//Removed booking so should be no check outs during given date
		assertTrue(fixture.listCheckOutsForDate(per.getEnd()).isEmpty());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#addExtraCostsForRoom(int, int, java.lang.String, double) <em>Add Extra Costs For Room</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#addExtraCostsForRoom(int, int, java.lang.String, double)
	 * @generated NOT
	 * Tests 2.1.13
	 */
	public void testAddExtraCostsForRoom__int_int_String_double() throws ParseException {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		sm.startup(1);
		
		int bookingID = fixture.initiateBooking("Elin", "20161111", "20161119", "Bjornsson");
		fixture.addRoomToBooking("default", bookingID);
		
		Reservation reservation = sm.getBookingmanager().getBookings().get(0).getReservations().get(0);
		fixture.addExtraCostsForRoom(bookingID, reservation.getReservationID(), "vodka", 149.5);
		fixture.addExtraCostsForRoom(bookingID, reservation.getReservationID(), "cigarettes", 45.2);
	
		assertEquals(194.7 ,  reservation.getExtraCost());
		assertEquals("vodka" ,  reservation.getDescriptionOfExtra().get(0));		
		assertEquals("cigarettes" ,  reservation.getDescriptionOfExtra().get(1));	
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#checkInBooking(int) <em>Check In Booking</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#checkInBooking(int)
	 * @generated NOT
	 * 2.1.3 - CHECK IN BOOKING
	 */
	public void testCheckInBooking__int() throws ParseException {
		StartupManager sm = Group17Factory.eINSTANCE.createStartupManager();
		sm.startup(1);
		BookingManager bm = sm.getBookingmanager();

		assertTrue(bm.initiateBooking("", "20161111", "20161119", "Bjornsson") < 1); // Empty first name
		assertTrue(bm.initiateBooking("Foo", "20161111", "20161119", "") < 1); // Empty last name
		assertTrue(bm.initiateBooking("", "20161111", "20161119", "") < 1); // Empty first/last name
		assertTrue(bm.initiateBooking("Foo", "20160102", "20160101", "Bar") < 1); // Start date > end date
		
		int bookingID = bm.initiateBooking("Elin", "20161111", "20161119", "Bjornsson");
		assertFalse(bm.addRoomToBooking("nonexistent-type", bookingID)); // Invalid room type
		assertTrue(bm.addRoomToBooking("default", bookingID));
		assertFalse(fixture.addRoomToBooking("default", bookingID));
		

		Reservation reservation = bm.getBookings().get(0).getReservations().get(0);
		assertNotNull(reservation);
		
		assertEquals(ReservationStatus.CREATED, reservation.getStatus());
		bm.confirmBooking(bookingID);
		assertEquals(ReservationStatus.CONFIRMED, reservation.getStatus());
		assertEquals("default", reservation.getRoomType().getName());
		bm.checkInBooking(bookingID);
		assertEquals(ReservationStatus.CHECKEDIN, reservation.getStatus());
		assertEquals("default", reservation.getRoom().getRoomType().getName());
		
		// When checked in, pre-assigned room type should be null
		assertNull(reservation.getRoomType());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date, java.util.Date) <em>List Check Ins For Date</em>}' operation.
	 * @throws ParseException 
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckInsForDate(java.util.Date, java.util.Date)
	 * @generated NOT
	 * Tests UC 2.1.9
	 */
	public void testListCheckInsForDate__Date_Date() throws ParseException {
		//Basic test, one check in during one period should assert true
		Period per = new PeriodImpl("20160101","20160108");
		assertTrue(fixture.listCheckInsForDate(per.getStart(), per.getEnd()).isEmpty());
		
		int bookingId = fixture.initiateBooking("John", "20160103", "20160104", "Doe");
		RoomManagerImpl rm = (RoomManagerImpl) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		fixture.addRoomToBooking("A", bookingId);
		fixture.confirmBooking(bookingId);

		for(Booking booking : fixture.listBookings()){
			if(booking.getBookingID()== bookingId){
				assertTrue(fixture.listCheckInsForDate(per.getStart(),per.getEnd()).contains(booking));
			}
		}
		fixture.cancelABooking(bookingId);
		assertTrue(fixture.listCheckInsForDate(per.getStart(), per.getEnd()).isEmpty());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date, java.util.Date) <em>List Check Outs For Date</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#listCheckOutsForDate(java.util.Date, java.util.Date)
	 * @generated NOT
	 * Tests UC 2.1.10
	 */
	public void testListCheckOutsForDate__Date_Date() {
		//Basic test, one check in during one period should assert true
		Period per = null;
		try {
			per = new PeriodImpl("20160101","20160108");
		} catch (Exception e) {
			e.printStackTrace();
		}
		assertTrue(fixture.listCheckOutsForDate(per.getStart(), per.getEnd()).isEmpty());
		int bookingId = fixture.initiateBooking("John", "20160103", "20160104", "Doe");
		RoomManagerImpl rm = (RoomManagerImpl) fixture.getIroomavailabilitymanagerprovides();
		rm.addRoomType("A", 0, 2, "");
		rm.addRoom(0, "A");
		fixture.addRoomToBooking("A", bookingId);
		fixture.confirmBooking(bookingId);

		for(Booking booking : fixture.listBookings()){
			if(booking.getBookingID()== bookingId){
				assertTrue(fixture.listCheckOutsForDate(per.getStart(),per.getEnd()).contains(booking));
			}
		}
		fixture.cancelABooking(bookingId);
		assertTrue(fixture.listCheckOutsForDate(per.getStart(), per.getEnd()).isEmpty());
	}

	/**
	 * Tests the '{@link se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#clearBookings() <em>Clear Bookings</em>}' operation.
	 * @see se.chalmers.cse.mdsd1617.group17.IBookingManagerProvides#clearBookings()
	 * @generated NOT
	 */
	public void testClearBookings() {
		BookingManager bm = new BookingManagerImpl();
		
		assertTrue(bm.initiateBooking("Waldo", "20161201", "20161224", "Odlaw") != -1);
		assertTrue(bm.initiateBooking("Donald", "20161201", "20161224", "Trump") != -1);
		assertTrue(bm.initiateBooking("Barrak", "20161201", "20161224", "Obama") != -1);
		
		assertEquals(3, bm.getBookings().size());
		
		bm.clearBookings();
		
		assertEquals(bm.getBookings().size(), 0);
	}

} //BookingManagerTest
